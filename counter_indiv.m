function [ctri] = counter_indiv(i)

% will return a single *text string* formatted to 8 printing characters
% makes it easy to have a single line on a screen display the current
% iteration
%
% note: only works on numbers up to eight digits (99,999,999)
%
% to clear each line: "fprintf('\b\b\b\b\b\b\b\b')"
%
% version = 2013.02.14
% RJE
    
if i < 10
   pre = '       ';

elseif i < 100
   pre = '      ';

elseif i < 1000
   pre = '     ';

elseif i < 10000
   pre = '    ';

elseif i < 100000
   pre = '   ';

elseif i < 1000000
   pre = '  '; 
   
elseif i < 10000000
   pre = ' ';
   
elseif i < 100000000
   pre = '';
end

ctri = [pre num2str(i)];


