function saved(funcname)

% saved funcname
% will return the date and time at which 'funcname' was last saved
%
% RJE | 2013.03.20


location = which(funcname,'-ALL');

nfiles = size(location,1);

save_date = cell(nfiles,1);

if sum(size(location)) == 0
    % not in the path
    fprintf(['\n Error: function <' funcname '> is not on the path.\n\n'])
    return

else
    fprintf('\n Files listed in order of path precedence: \n');
    for f = 1:nfiles
        file_info = dir(location{f});
        save_date{f} = file_info.date;

        fprintf(['\n   ' num2str(f) '. Location: '])
        disp(location{f})
        fprintf(  '         Saved: ')
        disp(file_info.date) 
        
        if f > 1
           date_match(f-1) = strcmp(save_date{f-1},save_date{f}); % will return "1" if this pair matches 
        end
        
    end
end


% reconcile files if more than one save date?
if nfiles > 1            
    
    if sum(date_match) == (nfiles - 1);
        % all have same date
        recon = 2;
        fprintf('\n All files have the same saved date/time stamp. \n');
    else
        recon = input('\n Reconcile files to most recent saved date?: \n   [1] Yes \n   [2] No <ENTER> \n   --> ');
    end
    
    if isempty(recon)
        recon = 2;
    end
    
    if recon == 1
        % first find the most recent date (descending order)
        
        [x,ind] = sort(datenum(save_date),'descend'); % index returns the index of the original file order

        % now we simply replace files 2:nfiles with the first one
        for f = 2:nfiles
            copyfile(location{ind(1)},location{ind(f)}); % copies the first (most recent) file to the other locations
        end
        fprintf('\n All files reconciled.\n');
        
    elseif recon == 2
        % do nothing
    end
    
end

fprintf('\n') % keep this



