function [res_rows res] = beats_calc(beats,beats_conf,bars,bars_conf,ex_inds,fname,plot_trim,pause_sec,local_thr,string_type,run_dur_thr,gap_dur_thr,prc,plot_color)

% function [res_rows res] = calc_outcomes(beats,beats_conf,bars,bars_conf,win,fname,local_thr,plot_trim,pause_sec,run_dur_thr,gap_dur_thr,prc,plot_color)
% 
% Note: this function works on a SINGLE song only
%
% beats = a timestamp series of beats (NOT a difference series!)
% bars  = a timestamp series of bars (measures)
% win   = optional start and stop window values for each file (N x 2 matrix)
% name  = file number just for plotting purposes
%
% prc = a vector of percentiles (e.g., [10 50 90] to evaluate for the
% percentile-based variability measures
%
% measures calculated (via td_calcs_concat): CV, PADM, PASD, PCI
%
% [res] will be a [1 x N] vector
%
% The equations here assume that "double taps" have already been eliminated
%
% rje | version = 2013.04.10
%


%% can we plot in color (= 1) or black and white (= 0)?



if isempty(ex_inds)
   ex = [];
else

   ex = ex_inds;
end 


% make sure just real values
beats = beats(beats > 0); % gets rid of zero and NaN cells
bars = bars(bars > 0);

% make sure we actually HAVE beats and bars

% take the diff
diff_beats = diff(beats);
diff_beats = diff_beats(:); % column

diff_bars = diff(bars);
diff_bars = diff_bars(:);

%% to make it easier to plot the x-axis (timestamps)
beats = beats(2:end); 
bars = bars(2:end);

% basic details
nint = numel(diff_beats);
ind = 1:nint;

if isempty(fname)
    fname = 'Data';
end

%% something new - evaluating the stability of a beat series
% ignore the first 10 seconds of a piece; then take excerpts of [10:10:120]
% and see what the 25, 50, and 75 percentiles are

dur_check = [2 4 8 16 32 64 128];
%dur_vs_spread_real = nan(1,numel(dur_check));
%dur_vs_spread_fake = nan(1,numel(dur_check));

% cut off first 10 seconds
diff2 = diff_beats(beats > 10);
beats2 =     beats(beats > 10);
beats2 = beats2 - min(beats2); % so that the first time-stamp is at t = 0 for simplicity

%fake = randn(numel(diff2),1)*std(diff2)+median(diff2); % to match the properties of the actual series

for d = 1:numel(dur_check)
    tmp = diff2(beats2 <= dur_check(d));
    %rnd = fake(beats2  <= dur_check(d));
    
    if numel(tmp) >= 2
        %tmp = tmp(:); % important; prctile_nist requires column format

        %tmp_prc = prctile_nist(tmp,prc);
        %rnd_prc = prctile_nist(rnd,prc);

        %dur_vs_spread_real(d) = 100 * (tmp_prc(3) - tmp_prc(1)) / tmp_prc(2);
        %dur_vs_spread_fake(d) = 100 * (rnd_prc(3) - rnd_prc(1)) / rnd_prc(2);

    elseif numel(tmp) < 2;
        %dur_vs_spread_real(d) = 0; % also works for []
    end
end



% now we have to normalize it
%dur_vs_spread_real = dur_vs_spread_real / min(dur_vs_spread_real);

% which is the min and max position?
%dur_vs_spread_min = dur_vs_spread_real == min(dur_vs_spread_real);
%dur_vs_spread_max = dur_vs_spread_real == max(dur_vs_spread_real);

%figure(40)
%plot(dur_vs_spread_real)
%hold on

%aa = dur_vs_spread_real - dur_vs_spread_fake
%bb = 0.5 * (dur_vs_spread_real + dur_vs_spread_fake)
%dur_vs_spread_diff = 100 * aa ./ bb 


%% outlier calculation

ex_cap = 0; % don't cap the ends (makes for more accurate plotting)
ex_method = 3; % RJE auto method
ex_inds_import = [];
del_itis = 0; % this value should be zero to avoid confusion
long_run_method = 's'; % single longest run
loc_meth = 'kde_botev'; % other options: 'bf' for RJE brute force method; 'kde_botev'; 'mh' for midhinge
plot_it = 0;
fnum = NaN;
outlier_meth = 'pc';
outlier_thr = local_thr;
do_slope = 1;

[Xmat stats] = td_outlier_detect(beats,[],ex_cap,ex_method,ex_inds_import,del_itis,long_run_method, loc_meth, outlier_meth, outlier_thr, string_type, run_dur_thr, gap_dur_thr, do_slope, plot_it,fnum,fname);
% note: Xmat will be smaller than input if ex_cap > 0

good_ind = stats.good_indices; % the actual index values
location = stats.location;
gap_dur_sum = stats.gap_dur_sum;

if isempty(good_ind) % no valid indices
    good_ind = [1 1];   % just to make code work ...
    do_calc = 0;        % ... but calculations are invalid

else
    do_calc = 1; % calulations are valid
end
    
% Override: if we are doing manual trimming, then we override the do_calc option
if plot_trim == 3
   do_calc = 1;
end

%% plot 1 of 4
    if plot_trim == 2 || plot_trim == 3

        %loc_compare(diff_beats,1);
        
        h = figure(100);
        set(h,'Color','w');
        clf % clear figure
        
        subplot(4,1,[1 2])

        % plot the IBIs as a function of time to make it easier to compare
        % with the actual audio file. will need to change time into index
        % values subsequently
        
        % a line showing the calculated central tendency of the beat series, just for reference

        if plot_color == 1
             plot(beats,diff_beats,'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])
             hold on
             plot([0 max(beats)],[location location],'b','LineWidth',1) % max(beats) is the highest x-value
             
             % best run
             plot(beats(good_ind),diff_beats(good_ind),'MarkerFaceColor',[0 0.8 0],'MarkerEdgeColor',[0 0.8 0],'Marker','o','MarkerSize',3,'Color',[0 0 0],'LineStyle','none')
             

        else
             plot(beats,diff_beats,'MarkerFaceColor',[1 1 1],'MarkerEdgeColor',[0 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])
             hold on
             plot([0 max(beats)],[location location],'Color',[0.5 0.5 0.5],'LineWidth',1)
             
             % best run
             plot(beats(good_ind),diff_beats(good_ind),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0],'LineStyle','none')
             
        end
        
        
        ylabel('IBeI (s)')
        xlabel('Time (s)')
        title(fname,'Interpreter','none')

    end    

    

%% manual trimming
if plot_trim == 3
    
        % Now we need to VISUALLY select the starting and stoping indices 
        % (independent of the other scrubbing methods)

        % ************
        % Note: although outliers are VISUALIZED, they are only EXCLUDED
        % after the following step is complete; skipping this step will
        % only exclude the end caps


        title([fname '. Use the mouse to select the START and STOP times'])
        [ex, y] = ginput(2); % a START and a STOP value

        ex = round(ex);
        
        % important: need to get these into *index* values
        ex_ind(1) = min(ind(beats >= ex(1))); % the first index *after* the *first* click
        ex_ind(2) = max(ind(beats <= ex(2))); % the last index *before* the *second* click

        
        if ex_ind(2) > nint
           ex_ind(2) = nint; % just so we don't have problems
        end

        trim_ind = ex_ind; % now we will have it as a row: ex_ind = [start stop]
         
else
    % use the automatically detected index range!
    ex_ind = [min(good_ind) max(good_ind)];
    trim_ind = ex_ind; % output variable
    
end % if plot_trim

% ===================
% for all methods ...
% the actual onset and offset timestamps (of the file)
trim_time = [beats(ex_ind(1)) beats(ex_ind(2))]; % valid for both beats and bars

% ===================
% TRIMMING
% beats
diff_beats_seg = diff_beats(ex_ind(1):ex_ind(2));
diff_beats_seg = diff_beats_seg(:);
     beats_seg = beats(ex_ind(1):ex_ind(2)); % the ORIGINAL time stamps
     
     beats_seg = beats_seg(:); % column

% ===================     
% td_calc script is a general collection of time-domain calculations
loc_val = stats.location; % for simplicity, we use the location identified from the *entire* segment during td_outlier_detect
outlier_meth = 'pc';
do_dfa = 0; % don't do DFA
num_events = Inf; % use all available data
plot_it = 0;

% note: we can't do slope calculations here, since GAPS are included! Slope
% is calculated in robustfit_slope, as part of long_run_ts, and passed back
% to td_outlier_detect
td_out = td_calcs_concat(beats_seg, prc, loc_val, outlier_meth, do_dfa, num_events, plot_it); % beats_seg is just the good data after identifying outliers, etc

% beat confidence
if isempty(beats_conf)
    beats_conf_seg = NaN;
else
    beats_conf_seg = beats_conf(ex_ind(1):ex_ind(2)+1);
end

% bars
mkeep_low = bars >= trim_time(1); 
mkeep_high = bars <= trim_time(2); 

mkeep = mkeep_low + mkeep_high; 

diff_bars_seg = diff_bars(mkeep == 2); % y-axis bar values
     bars_seg = bars(mkeep == 2); % the ORIGINAL time stamps

% bars confidence
if isempty(bars_conf)
    bars_conf_seg = NaN;
else
    bars_conf_seg = bars_conf(mkeep == 2);
end

% get the values to plot slope
x_start = stats.plot_slope_points.x_start;
x_end = stats.plot_slope_points.x_end;
y_start = stats.plot_slope_points.y_start;
y_end = stats.plot_slope_points.y_end;

 % now add the slope
 if plot_trim == 2 || plot_trim == 3
	subplot(4,1,[1 2])
    hold on
    
    if plot_color == 1
        for i = 1:numel(x_start)
            plot([x_start(i) x_end(i)],[y_start(i) y_end(i)],'r','LineWidth',1)  
        end         
    else
        for i = 1:numel(x_start)
            plot([x_start(i) x_end(i)],[y_start(i) y_end(i)],'Color',[0.502 0.502 0.502],'LineWidth',2)  
        end        
    end 
    
    hold off
 end
%% plot 2 of 4
if plot_trim == 2 || plot_trim == 3
    % now replot the series for simplicity
    
    subplot(4,1,3)
    if do_calc == 1
        %beats_time_seg = cumsum(diff_beats_seg); % for x-axis
        %bars_time_seg = cumsum(diff_bars_seg); % x-axis
        
        % insert the slope of the robust regression line underneath the data
%         xvals = td_out.xvals;
%         yvals = td_out.yvals;

        
        if plot_color == 1
            plot(beats_seg(2:end),diff(beats_seg),'MarkerFaceColor',[0 0.8 0],'MarkerEdgeColor',[0 0.8 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])
        else
            plot(beats_seg(2:end),diff(beats_seg),'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])    
        end
            hold on
            %title(fname,'Interpreter','none')
            ylabel('IBeI (s)')
            xlabel('Time (s)')

        if plot_color == 1
            for i = 1:numel(x_start)
                plot([x_start(i) x_end(i)],[y_start(i) y_end(i)],'r')  
            end         
        else
            for i = 1:numel(x_start)
                plot([x_start(i) x_end(i)],[y_start(i) y_end(i)],'Color',[0.502 0.502 0.502],'LineWidth',2)  
            end        
        end 

    end
    
end



% the length of the segment  
seg_dur = max(beats_seg) - min(beats_seg); % this is the most direct way; we DON'T want cumsum 

% relative statistics; round to the nearest 100th of percent
% note: this method won't work if the whole file is used, but that won't be
% diagnostic anyway!

% we get integer values
rel_start = ceil((min(beats_seg) - beats(1)) / (beats(end) - beats(1)) * 100);
rel_end   = floor((max(beats_seg) - beats(1)) / (beats(end) - beats(1)) * 100);

% total length of the file from 1st to last beat
beats = beats(beats > 0); % just to confirm
total_dur = beats(end) - beats(3); % note: we use beats(3) because this is the first actual value that we can assess using our outlier detection method

% need to adjust slightly if we are using the whole excerpt
if seg_dur > total_dur
    seg_dur = total_dur;
end

%% measures to be calculated

if do_calc == 1

    stable_perc = (seg_dur / total_dur) * 100;  % the percentage of the total file that is stable
                                                % confirmed that this will = 100 if the full segment is used
    run_perc = 100 * (seg_dur - gap_dur_sum) / seg_dur;                             
    rate_md = 60 / median(diff(beats_seg)); % based on the median to make this stable
    rate_mn = 60 / mean(diff(beats_seg));   % based on mean; a less robust measure!
    rate_loc = 60 / loc_val; % based on the location as calculated during outlier detection
    
    % new as of 2013.04.27: need to redefine meter
    nbe = numel(beats_seg);
    nba = numel(bars_seg);
    
    bar_err = nan(nba,1);
    bar_rnd = nan(nba,1);
    be_per_ba = nan(nba,1);
    
    for k = 1:nba % makes it slightly easier
        bl = max(beats_seg(beats_seg <= bars_seg(k)));
        bh = min(beats_seg(beats_seg >= bars_seg(k)));
        
        if isempty(bl)
            bl = Inf; % easier to work with than NaN here
        end
       
        if isempty(bh)
            bh = Inf;
        end
        
        
        % we also want to see, for every barline, how off it is compared to
        % the nearest beat. This does NOT seem to be relevant for the Echonest
        % data, but seems to be an issue for the Klapuri algorithm
        dl = abs(bars_seg(k) - bl); % we don't care about sign, just absolute error
        dh = abs(bh - bars_seg(k));
        
        % a 'rounded' bar series
        if dl < dh
           bar_rnd(k) = bl;
        else
           bar_rnd(k) = bh;
        end
        
        % the closer of these two will be part of the parameter
        bar_err(k) = min(dl,dh); % all values will be >= 0
        
    end
    
    % take the 90th percentile of error values (since it is an absolute
    % value series)
    
    if numel(bar_err) > 5; % there were a couple errors in the MSD that necessitated adding this rule
        bar_err_95p = prctile_nist(bar_err,90);
    else
        bar_err_95p = NaN;
    end
    
    % next, we count the number of beats between each rounded bar
    for k = 1:(nba-1);
        bh = beats_seg(beats_seg >= bar_rnd(k));
        bl = beats_seg(beats_seg < bar_rnd(k+1)); % < not <=
        
        be_per_ba(k) = numel(intersect(bh,bl)); % will be 0 if there is no intersection
    end
    
    % because we could have a meter that changes over the course of the
    % file, we use mean as the summary statistic
    
    
    be_per_ba = be_per_ba(1:(end-1));  % last value will be NaN
    
    meter_est = mean(be_per_ba);
    
    % also to be safe, need to check min and max
    
    if mod(meter_est,1) == 0 % true if we have .00000
       
       if min(be_per_ba) == max(be_per_ba)
           % still ok
       else
           % error
           meter_est = NaN;
       end
    
    end
    
    % old version
    % meter_est = round( median(diff_bars_seg) / median(diff_beats_seg) ); % very simple: just round the median bar length divided by the median beat length
    
    % 10th percentile of confidence values; i.e., 90% of values are >= this value
    if numel(beats_conf_seg) < 10
        beats_conf_prc = NaN;
    else
        beats_conf_prc = prctile_nist(beats_conf_seg,5); 
    end

    if numel(bars_conf_seg) < 10
        bars_conf_prc = NaN;
    else
        bars_conf_prc = prctile_nist(bars_conf_seg,5);
    end
    
% RJE phi method: a single timestamp series
% note: this is now done in pci_calc_concat, but is not relevant for the BEATS
% proejct (leave the values here for placeholders)

    pci_mn = nan;
    pci_md = nan;
    
elseif do_calc == 0
    stable_perc = nan; % just so we aren't confused
    run_perc = nan;
    rate_md = nan; 
    rate_mn = nan;
    rate_loc = nan;
    bar_err_95p = nan;
    meter_est = nan;
    seg_dur = nan;

    beats_conf_prc = nan;
    bars_conf_prc = nan;

    pci_mn = nan;
    pci_md = nan;   

end % do_calc



%% look for more outliers outliers, now using the median method
%     seg_med  = median(diff(beats_seg));
%     seg_mdad = prctile_nist((abs(diff(beats_seg) - seg_med)),50); % median absolute deviation; has the potential to be 0, making this a BAD choice!
%     seg_mnad = mean(abs(diff(beats_seg) - seg_med));             % will be larger than mdad (rje checked)
%     
%     seg_thr_hi = seg_med + 3 * seg_mnad; % use median measures to be more robust; MAD(x) will be smaller than STD(x)
%     seg_thr_lo = seg_med - 3 * seg_mnad;      
% 
%     hi_out_ind2 = ind(diff(beats_seg) > seg_thr_hi); 
%     lo_out_ind2 = ind(diff(beats_seg) < seg_thr_lo); 
%     hi_out_val2 = (diff(beats_seg) > seg_thr_hi); 
%     lo_out_val2 = (diff(beats_seg) < seg_thr_lo);
% 
%     ind_all_seg = [hi_out_ind2'; lo_out_ind2'];
%     ex_std = ind_all_seg';
% 
% 
%     ex_vals_seg = [hi_out_val2; lo_out_val2]; % already in rows
%     

if plot_trim == 2 || plot_trim == 3

   if do_calc == 1
       
   % replot the slope for clarity     
%         if isnan(xvals(1))
%            % don't add the line
%         else
%            if plot_color == 1
%                plot([xvals(1) xvals(2)],[yvals(1) yvals(2)],'Color',[1.0 0.6 0.2],'LineWidth',2); % orange line
%            else
%                plot([xvals(1) xvals(2)],[yvals(1) yvals(2)],'Color',[0.5 0.5 0.5],'LineWidth',2); % grey line
%            end
%         end
        hold off
          
   end
   %% final subplot
   subplot(4,1,4) % always show even if we don't plot
   % new: just plot beats per bar!
   
   if do_calc == 1
       if plot_color == 1
           plot(bars_seg(1:end-1),be_per_ba,'MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[1 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])
       else
           plot(bars_seg(1:end-1),be_per_ba,'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'Marker','o','MarkerSize',3,'Color',[0 0 0])
       end
       
       xlabel('Time (s)')
       ylabel('Beats per Bar')
       
%        if plot_color == 1
%             plot(beats_seg,ones(numel(beats_seg)),'b.')
%             hold on
%             plot(bars_seg,ones(numel(bars_seg)),'ro')
%             hold off
%        else
%             plot(beats_seg,ones(numel(beats_seg)),'k.')
%             hold on
%             plot(bars_seg,ones(numel(bars_seg)),'ko')
%             hold off
%        end
%                 %title(fname,'Interpreter','none')
% 
%                 xlabel('IBeI (dot) vs. IBaI (circle)')
                
   end
    
    % histogram
    %subplot(5,1,5) % always show even if we don't plot
        
   if do_calc == 1
       % use RJE code to make the bin plotting optimal
       %plot_type = 'b';
       %log_it = 1;
       %fig_num = 0;
       % histc_gen(diff_beats_seg,plot_type,10,10,1,log_it,fig_num);
       % histc_gen(X,plot_type,num_max,num_min,height_min,log_it,fig_num,cur_plot,num_plot)
       
        %ylabel('Count')
        %xlabel('IBeI histogram bins')
   end
   
   if pause_sec == Inf;
       pause();
   else
       pause(pause_sec);
   end

end


%% outputs

% individual variable names
res.trim_ind = trim_ind;   % the starting and stopping indices (2 cols)
res.trim_time = trim_time; % starting and stopping times (2 cols)
res.seg_dur = seg_dur;
res.stable_perc = stable_perc;
res.run_perc = run_perc;
res.rel_start = rel_start;
res.rel_end = rel_end;
%res.run_meth = run_meth; % 1 = median; 2 = mode; 3 = locstat

% rate data
res.rate_md = rate_md;
res.rate_mn = rate_mn;
res.rate_loc = rate_loc;
res.bar_err_95p = bar_err_95p;
res.meter_est = meter_est;
res.beats_conf_prc = beats_conf_prc;
res.bars_conf_prc = bars_conf_prc;

% will be empty for BEATS
res.pci_mn = pci_mn;
res.pci_md = pci_md;

%res.dur_vs_spread = dur_vs_spread_real;
%res.dur_vs_spread_min = dur_vs_spread_min;
%res.dur_vs_spread_max = dur_vs_spread_max;


% variability outputs from td_calc

if do_calc == 1
    
    res.slope_pc_max_abs = stats.slope_pc_max_abs; % comes from td_outlier_detect
    %hi = stats.slope_pc_max_abs
    %pause(2)
    
    res.cvsd  = td_out.cvsd;
    res.cvrms = td_out.cvrms;

    res.percentiles = prc; % write this out for clarity
    res.loc_prc   = td_out.loc_prc; % rows
    res.loc_transf_max = stats.loc_transf_max; % comes from td_outlier_detect, *not* td_calcs_concat
    res.suc_prc   = td_out.suc_prc; % rows
    res.suc_transf_max = stats.suc_transf_max; % comes from td_outlier_detect, *not* td_calcs_concat
    %res.dfa_alpha = td_out.dfa_alpha;
    
elseif do_calc == 0
    % we do this just to avoid taking estimates of the first data point

    res.slope_pc_max_abs = nan;
    res.cvsd = nan;
    res.cvrms = nan;
    
    res.percentiles = prc;
    res.loc_prc  = nan(1,numel(prc));
    res.loc_transf_max = nan;
    res.suc_prc  = nan(1,numel(prc));
    res.suc_transf_max = nan;
    
    %res.dfa_alpha = nan;
end


%% output
% a single row for easy batch use
run_meth = NaN; % we don't do this comparison now

% note: we just paste in things from the output of td_calcs_concat ("res") above
% 2013.10.04 - make this output simpler (fewer fields)
%res_rows = [trim_ind trim_time seg_dur stable_perc run_meth rate_md rate_mn meter_est bar_err_95p beats_conf_prc bars_conf_prc pci_mn pci_md res.slope_pc_max res.cvsd res.cvrms prc res.loc_prc res.suc_prc];
 res_rows = [trim_ind trim_time seg_dur stable_perc run_perc rate_loc meter_est beats_conf_prc bars_conf_prc res.slope_pc_max_abs res.loc_transf_max res.suc_transf_max];
