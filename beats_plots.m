function output = beats_plots(nplots,nbins,plot_type,log_it)

% function beats_plots(nplots,nbins,plot_type,log_it)
%
% "plot_type": 'b' for bar, 'l' for line
% "log_it": 0 = no, 1 = yes

% John Cai and Rob Ellis

[filenames, pathname] = uigetfile( {'*main*','BEATS main files'},'Select desired files','MultiSelect', 'on');


if iscell(filenames)
    % cell
    filenum = size(filenames,2);
    %numlines = input([' Read in ' num2str(filenum) ' files. How many distinct plot lines?: ']);
    numlines = 1; % keep this simple for now
    
    if numlines > 1
        % seems like we need to sort this
        filenames = sort(filenames);
    end
    
else
    % string
    filenum = 1;
    numlines = 1;
end

fprintf(['\n Read in ' num2str(filenum) ' files. Working ...'])

% now combine all the data
for i = 1 : filenum
    
    if iscell(filenames)
        tmp = load([pathname filenames{i}]);
    else
        tmp = load([pathname filenames]);
    end
    
    if i == 1
        results = tmp.main.results; % establish this variable
        msd_vs_beats = tmp.main.msd_vs_beats;
    elseif i > 1
        results = [results; tmp.main.results]; % put *all* the files into this matrix
        msd_vs_beats = [msd_vs_beats; tmp.main.msd_vs_beats];
    end
    
end

cd(pathname);

if nplots == 8
    
    plotName = {'Stable Duration (s)','Stable Percentage (%)','Estimated Tempo (bpm)','Delta Slope (%)','Beats-per-Bar','Alignment Error (s)','PADL (%)','PASD (%)'};
    plotID = [7,8,10,18,12,13,26,29];

elseif nplots == 6
    plotName = {'Stable Duration (s)','Stable Percentage (%)','Estimated Tempo (bpm)','Estimated Meter (beats per bar)','PADL_9_0 (%)','PASD_9_0 (%)'};
    plotID = [7,8,10,12,26,29];


elseif nplots == 9
    % updated 2013.10.15
    plotName = {'Stable Duration (s)','Stable Percentage (%)','Run Percentage (%)','Estimated Tempo (bpm)','Estimated Meter (beats per bar)','PDM_m_a_x (%)','PSC_m_a_x (%)','PTD_m_a_x','ETM (%)'};
    plotID = [7,8,9,10,11,15,16,14,17];
end

% **********************************
% HISTOGRAMS
h = figure(300);
set(h,'Color','w');

for i = 1:nplots
    
    if nplots == 6
       subplot(3,2,i);
    elseif nplots == 8
       subplot(4,2,i);
    elseif nplots == 9
       subplot(3,3,i); 
    end
        
    for j = 1:numlines
        % rje wrote this function to make histogram/bar plots easier
              
        data = results(:,plotID(i)); % NaN will be removed in histc_gen
        
        % restrict range a bit for stable duration
        if plotID(i) == 7
            data = data(data<1500);
        end
        
        cur_plot = j;
        num_plot = numlines;
        histc_gen(data,log_it,plot_type,nbins,nbins,1,0,cur_plot,num_plot); % force everything to have the same number of bins
       
        
        if j == 1
            hold on
        end
        
        if j == numlines
            hold off
        end

        xlabel(plotName{i});
        
    end
    
end

% **********************************
% Percentage of files >= x-value

% Stable Percentage
%data1 = results{1};
sp = results(:,8); 
total_files = numel(sp);

sp = sp(sp >= 0); % cut out NaN

sp_x = 5:5:100; sp_x = sp_x(:);
sp_y = zeros(numel(sp_x),1);

for i = 1:numel(sp_x);
    sp_y(i) = sum(sp >= sp_x(i));
end

sp_y = 100* sp_y / total_files;

% stable duration
sd = results(:,7); 
sd = sd(sd >= 0); % cut out NaN

sd_x = 60:20:240; sd_x = sd_x(:);
sd_y = zeros(numel(sd_x),1);

for i = 1:numel(sd_x);
    sd_y(i) = sum(sd >= sd_x(i));
end

sd_y = 100* sd_y / total_files;

figure(301)
subplot(1,2,1)
plot(sp_x,sp_y)

subplot(1,2,2)
plot(sd_x,sd_y)


% % **********************************
% % cumulative distribution function
% h = figure(302);
% set(h,'Color','w');
% 
% % set colors for hist
%  cc = gray(numlines+1); % last one will be white [1 1 1]
%  cc = cc(1:end-1,:);
%  
%  cc = sortrows(cc,-1);
%  
% %cc = hsv(numlines); % for color
%         
%  for j = 1:numlines
%     data1 = result{j};
%     
%     data = data1(:,7);  % look at absolute LENGTH, not percent stability (col 8)
%                         % also, this makes more sense, because we can have
%                         % a valid value for "stable length = 0 seconds"
%                         
%     data(isnan(data)) = 0; % i.e., nan means stable length = 0
%     
%     looks = linspace(min(data),max(data),100); % values to examine
%     nlooks = numel(looks);
%     
%     totdata = numel(data);
%     
%     %data(isnan(data)) = [];
%     %ndata = numel(data);' % adjusts on every loop
%     
%     yvals = zeros(nlooks,1);
%     
%     for k = 1:nlooks
%         %yvals(k) = sum(data <= looks(k)) / ndata; % CDF means <= to current value
%         yvals(k) = sum(data <= looks(k)) / totdata; % this is equivalent to cumulative sum
%     end
% 
%     % plot this
%     plot(looks,yvals,'LineWidth',3,'color',cc(j,:),'DisplayName',['Data ' num2str(j)]);
%     xlabel('Stable Length (s)')
%     ylabel('Proportion')
%     %legend('show');
% 
%         if j == 1
%             hold on
%         end
%         
%         if j == numlines
%             hold off
%         end
%     
%    
%  end

fprintf(' Finished.\n\n')

%% outputs
output.total_files = total_files;
output.sp_x = sp_x;
output.sp_y = sp_y;
output.sd_x = sd_x;
output.sd_y = sd_y;
output.results = results;
output.msd_vs_beats = msd_vs_beats;
