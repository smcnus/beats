
function [output] = beats_build

% to easily compile sections of a BEATS output and then extract relevant
% fields for further plots

% get the files

[filenames, pathname] = uigetfile( ...
       {'*main*','MAT-files (*.mat)'}, ...
        'Select the BEATS *main* outputs (up to 15)', ...
        'MultiSelect', 'on');

% change to that directory
cd(pathname);

% how many files?
if size(filenames,2) <= 15
    numf = size(filenames,2);
else
    numf = 1;
end

progressbar  % Initialize progress bar
p = 1; % starting counter

results = []; % start with empty
msd_vs_beats = []; 
echonest_file_info = [];

for i = 1:numf
    progressbar(p/numf)
    
    if numf == 1
        file = [pathname filenames];
    elseif numf > 1
        file = [pathname filenames{i}];
    end
    
    p = p + 1;
    
    data    = open(file);
    
    % build it
    results             = [results; data.main.results];
    msd_vs_beats        = [msd_vs_beats; data.main.msd_vs_beats]; % should have this structure
    echonest_file_info  = [echonest_file_info; data.main.echonest_file_info];
    
end
progressbar(1) % close it

% =====================================================================

% we need a NEW unique ID because we have built from sections

numf = size(results,1);

ind = 1:numf;
ind = ind(:);
ind_cell = num2cell(ind);



% get vector for Echonest tempo and BEATS tempo

tempo_e = msd_vs_beats(:,2);
tempo_b = msd_vs_beats(:,3);

% total files
tally.num_files = size(results,1);

% calculate the percent deviation - do this just in case we had the old version
% in the actual BEATS output
            % current version of ETM: simple percent deviation of BEATS
            % relative to ECHO; so that if ECHO = 60, then BEATS = 55 and
            % BEATS = 65 will have the same magnitude ETM

%mean_tempo =  0.5 * (tempo_e + tempo_b);

tempo_mismatch  =  msd_vs_beats(:,6); % pull this from BEATS output: (tempo_beats - tempo_echo) / tempo_echo


% how many files do not have a valid stable length?
stab_len = msd_vs_beats(:,9);
len_invalid = stab_len < 5; % 1 means invalid

tally.invalid_beats_len = sum(len_invalid);

% 2x2 table based on Echo valid tempo (0 = valid, 1 = invalid) and BEATS stable duration (0 = valid, 2 = invalid)?

x1 = tempo_e == 0;   % 1 if invalid
x2 = isnan(tempo_b); % 1 if invalid

%sum(x1)
%sum(x2)

tally.Einv_Binv = sum((x1 == 1 & x2 == 1));
tally.Einv_Bval = sum((x1 == 1 & x2 == 0));
tally.Eval_Binv = sum((x1 == 0 & x2 == 1));

final_include = (x1 == 0 & x2 == 0);
tally.Eval_Bval = sum(final_include);

% how many files have the same EXACT meter?
    meter_e = msd_vs_beats(:,4);
    meter_b = msd_vs_beats(:,5);

    % round beats to two decimals
    meter_b = round(meter_b * 100)/100;
    
    % difference
    meter_diff = meter_e - meter_b;
    
    % get rid of invalid files
    
    meter_diff = meter_diff(final_include == 1);
    
    % tally
    tally.meter_match_perc = 100 * sum(meter_diff == 0) / numel(meter_diff);


% how many files are missing an Echonest time_signature?
tally.invalid_echo_time_sig = sum(msd_vs_beats(:,4)==0);


% missing beat and bar confidence
be_con = isnan(results(:,12));
ba_con = isnan(results(:,13));

tally.Echo_Be_conf_missing = sum(be_con == 1);
tally.Echo_Ba_conf_missing = sum(ba_con == 1);

% get the full tally of excluded based upon tempo and stable duration

%full_ex = len_invalid + x1 + be_con + ba_con;
%tally.total_invalid = sum(full_ex >= 1);


% =========================================
% six-plot for MSD vs BEATS tempo
plot_eda(tempo_e,tempo_b,9,100,'Echo Nest tempo','BEATS tempo');

% scatter plot for PDL and PSC max

pdl_max = results(:,15);
psc_max = results(:,16);
ptd_max = results(:,14);

% get rid of empty
incl1 = pdl_max >= 0;
incl2 = psc_max >= 0;
incl = min(incl1,incl2);

pdl_max = pdl_max(incl);
psc_max = psc_max(incl);

figure(150)
%scatter3(pdl_max,psc_max,ptd_max,'*')
plot(pdl_max,psc_max,'*')
xlabel('PDL max')
ylabel('PSC max')
%zlabel('PTD max')

corr_pdl_psc = corr(pdl_max,psc_max);

% a figure similar to Bland Altman showing the percent deviation from echonest
figure(200)
plot(tempo_e,tempo_mismatch,'*')

hold on
    % error bars
    med_y = prctile_nist(tempo_mismatch,50);
    min_y = prctile_nist(tempo_mismatch,2.5);
    max_y = prctile_nist(tempo_mismatch,97.5);

    min_x = floor(min(tempo_e));
    max_x =  ceil(max(tempo_e));

    plot([min_x max_x],[min_y min_y],'r');
    plot([min_x max_x],[med_y med_y],'r','LineStyle',':');
    plot([min_x max_x],[max_y max_y],'r');

    xlabel('Echo Nest Tempo')
    ylabel('Est. Tempo Mismatch (%)')
    
hold off

tally.ETM_pLow  = min_y;
tally.ETM_pMed  = med_y;
tally.ETM_pHigh = max_y;

% another figure: a 2D version of Estimated Tempo Mismatch
% etm = msd_vs_beats(:,6);
% tmin = min(tempo_b,tempo_e);
% 
% figure(300)
% plot(tmin,etm,'*')
% 
% hold on
%     % error bars
%     etm_med = prctile_nist(etm,50);
%     min_y = prctile_nist(etm,2.5);
%     max_y = prctile_nist(etm,97.5);
% 
%     min_x = floor(min(tmin));
%     max_x =  ceil(max(tmin));
% 
%     plot([min_x max_x],[min_y min_y],'r');
%     plot([min_x max_x],[etm_med etm_med],'r','LineStyle',':');
%     plot([min_x max_x],[max_y max_y],'r');
% 
%     xlabel('Slower Tempo')
%     ylabel('Est. Tempo Mismatch')
%     
% hold off
% 
% stats.etm_low = min_y;
% stats.etm_med = etm_med;
% stats.etm_high = max_y;

%figure(400)
%plot(sort(tempo_mismatch-etm),'.')
%xlabel('Sorted cases')
%ylabel('Percent Diff - ETM')



% ================================
% outputs

output.tallies = tally;
output.results = results;
output.msd_vs_beats = msd_vs_beats;
output.echonest_file_info = echonest_file_info;
output.corr_pdl_psc = corr_pdl_psc;

