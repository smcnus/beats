function [output] = trim_batch_new(output)

% "trim_batch" will be the basis for a GUI used to interact with the output
% of beats.
%
% "output" is the output of beats.m, and has four internal structures

% pull out the relvant variables



% start
sub0 = output.results; % just redefine this to make it easier

% make a unique file index counter
fileind = 1:size(sub0,1);

% ****************************
% PLOTS

figure(10)

% how many bins do we want?
perbin = 20; % minimum number of items per bin
numf = size(sub0,1);

% plot by stable duration
subplot(1,5,1)
hist(sub0(:,7),round(numf/perbin));
xlabel('Stable duration (s)')

% plot by stable percentage (of total)
subplot(1,5,2)
hist(sub0(:,8),round(numf/perbin));
xlabel('Stable percentage (%)')

% plot by meter
subplot(1,5,3)
hist(sub0(:,10),round(numf/perbin));
xlabel('Meter')

% plot by PADM90
subplot(1,5,4)
hist(sub0(:,22),round(numf/perbin));
xlabel('PADM90')

% plot by PASD90
subplot(1,5,5)
hist(sub0(:,25),round(numf/perbin));
xlabel('PASD90')

% plot PADM vs PASD - correlation and Bland-Altman plot; will plot on figure(20)

ba_calc(sub0(:,22),sub0(:,25),1); % The "1" just means plot the result; this will automatically pull up another figure

% ****************************
% TRIM

% 1. trim by length
thr_len = 180;
ind_len = sub0(:,7) >= thr_len;

% 2. trim by percent stable
thr_sta = 50;
ind_sta = sub0(:,8) >= thr_sta;

% 3. trim by meter
thr_met = 4;
ind_met = sub0(:,10) == thr_met;

% 4. trim by PADM90
thr_padm = 2.0;
ind_padm = sub0(:,22) <= thr_padm;

% 5. trim by PASD90
thr_pasd = 2.0;
ind_pasd = sub0(:,25) <= thr_pasd;

% ********************************
% visualize a single file using calc_outcomes

% first, display all the files; let's say, based on PADM90
figure(15)
plot(sub0(:,22),'.') % we don't explicitly need x-axis values; this plot is designed to maximize visual efficiency

% make a label called "Zoom" with two radio buttons (On and Off) to allow the user to zoom
% when On is clicked, the following happens: 
title('Create a (rectangular) zoom window') % print this as the title on the figure
zoom

% when Off is clicked, the following happens
zoom off  % restores the cursor

% make a button called "Pin" to highlight a specific point
% when clicked, the following happens:
title('Pinpoint a single datapoint using the cursor')
[xind, y] = ginput(1); % a START and a STOP value

% whenever we sort, always sort [fileind xind] together!

xind = round(xind); % this is the *index* of the file. Now we go back and find the associated data from the results

% now we map from the *plotted* x-value to the *actual* file number, which
% is stored in the 2nd column of res_final_all

fileget = fileind(xind); % the unique file index


% get the beats and bars
beatsvar = output.beats_all{fileget}; % use name "beatsvar" so we don't accidentally call FUNCTION beats
bars = output.bars_all{filegt};

% the following parameters can just be left like this
beats_conf = []; bars_conf = []; win = []; plot_trim = 2;

% the following parameters should be *inputs* that the user can FLEXIBLY ADJUST

thr = 5;
run_length = 30;
gap_length = 3;
prc = 90; % just do a single value

% make a button called "Plot", which after it is clicked, will call the
% function below and make the plot

[res_rows res] = calc_outcomes(beatsvar,beats_conf,bars,bars_conf,win,fname,thr,plot_trim,run_length,gap_length,prc);

% Then, on screen, we can display the following: Length = , PADM = , PASD = 
% These fields will be populated by the output of calc_outcomes

stable_dur_disp = res.seg_dur;
stable_perc_disp = res.stable_perc;
padm_disp = res.padm;
pasd_disp = res.pasd;




% ********************************
% Final set of valid files; only those files which satisfy all 4 inclusion
% criteria (i.e., sum == 4) will be retained

ind_sum   = sum([ind_len, ind_sta, ind_met, ind_padm, ind_pasd],2); % sum the ROWS
ind_final = ind_sum == 5; % will be binarized

output.final_data = sub0(ind_final,:);
output.final_files = all_files(ind_final == 1);
