function [main xtra] = beats

% BEATS

%   1. select the target directory
%   2. identify the file types
%   3. extract the relevant parameters
%   4. run the beat stability analysis
% %   5. write a CSV file with results
%
% other functions that are called: 
%   progressbar - to visualize progress 
%   beats_calc - will take in a single file's worth of beats, measures,
%                   etc, and visualize the stable run
%   td_calcs_cum
%   prctile_nist
%
% RJ Ellis + Zhouhong CAI
% 2013 August

% **************************************************
% DEFAULTS
% these will populate the input variables below

% note: local threshold is defined with respect to using the *percent difference* calculation of outliers, and also of outcome measures
local_thr_def     = 5;     % quot value for SUCCESSIVE IBeIs and SUCCESSIVE RUNS
run_dur_def       = 10;    % run duration, in seconds (make it some multiple of 1.25: 2.5, 5.0, 10, 20, etc)
gap_dur_def       = 2.5;   % gap duration, in seconds (make it some multiple of 1.25: 2.5, 5.0, etc)

plot_color = 2;       % 1 = color; 2 = B&W


% **************************************************

%% hello

loc = which(mfilename); % note: must have a unique name and NOT an internal variable name
                      % in order to have the function called "beats", the
                      % internal variable must have a different name; i.e.,
                      % "beatsvar"
file_info = dir(loc);
save_date = file_info.date;

fprintf(['\n\n || BEATS: Basic Exploration of Auditory Temporal Stability \n || Version: ' save_date '\n || For more information, see: \n ||   http://m3r.comp.nus.edu.sg/wiki/index.php/Main_Page \n ||   http://robjellis.net \n']);


% check to see if we have robustfit toolbox in matlab (or other important
% functions?)

checkrob = which('robustfit'); % make sure we have it

if sum(size(checkrob)) > 0
   % OK
else
    fprintf('\n Warning: Could not find MATLAB "robustfit" function.\n\n')
end
    
%% identify file type 

fprintf('\n To-be-processed file type:')
ftype = input(['\n   [1] Echonest / Million Song DB (individual *.h5 files)',...
               '\n   [2] Echonest / Million Song DB (individual *.mat files) <ENTER>',...
               '\n   [3] Klapuri beats + bars (*.mat batch files)',...
               '\n   [4] *.csv',... 
               '\n   [5] *.txt',... 
               '\n   [6] XiaMi DB (part* batch files)',...
               '\n   --> ']);

if isempty(ftype)
    ftype = 2;
end

if ftype == 1
   tmatch = '.h5';
   datamsg = 'Select the parent directory of the *.h5 individual files';
elseif ftype == 2
   tmatch = '.mat';
   datamsg = 'Select the parent directory of the *.mat individual files';
elseif ftype == 3
   tmatch = '.mat';
   datamsg = 'Select the parent directory of the *.mat batch output files';
elseif ftype == 4
   tmatch = '.csv';
   datamsg = 'Select the parent directory of the *.csv files';
elseif ftype == 5
   tmatch = '.txt';
   datamsg = 'Select the parent directory of the *.txt files';
elseif ftype == 6
   tmatch = 'part';
   datamsg = 'Select the parent directory of the part* files';
elseif isempty(ftype)
   fprintf('\n\n Error: No file type selected. \n\n')
   return
end


%% select the target directory

input_mode = input('\n Input mode: \n   [1] GUI <ENTER> or \n   [2] Command line \n   --> ');

if isempty(input_mode)
   input_mode = 1;
end

if input_mode == 1
    inpath = uigetdir(pwd,datamsg);
elseif input_mode == 2
    inpath = input('   Enter the full path (no quotes): ','s');
    addpath(inpath)
end

% check to see if the last character is a filesep
if strcmp(inpath(end),filesep)
   % OK
else
   inpath = [inpath filesep]; % add filesep
end

cd(inpath); % this will be the output data directory


%% tally the files

fprintf(' \n Finding subdirectories ...');

% the following should work for ALL data types
showprog = 1; % display the ticker on screen

filenames = findAllFiles_rje(inpath,tmatch,showprog); % code by Dan Ellis + rje edit (to match any part of the file name)
fprintf('\n') % just leave this

numlist = numel(filenames); % this will work for datasets where numlist indicates *single* files, but will be redefined for batches


% a couple of special cases to deal with
if ftype == 2 % exclude .mat outputs from BEATS; all MSD songs start with TR****.mat
    ex_files = ones(numlist,1);
        
        for i = 1:numlist
            [pathname filename] = fileparts(filenames{i});
            if sum(findstr(filename,'BEATS')) > 0 % will match the string "BEATS" in caps
                ex_files(i) = 0; % move to zero
            elseif sum(findstr(filename,'ShowingContentIndex')) > 0 
                ex_files(i) = 0; % move to zero
            else
                % stay at one
            end

        end

    filenames = filenames(ex_files == 1); % will cut out "0"s
    numlist = numel(filenames);   
 
elseif ftype == 3 % Only include "klapuri_batch_output*.mat" files
    ex_files = ones(numlist,1);

        for i = 1:numlist
            [pathname filename] = fileparts(filenames{i});
            if sum(findstr(filename,'klapuri_batch_output')) > 0
                % stay at one
                
            else
                % exclude this case
                ex_files(i) = 0;
            end

        end

    filenames = filenames(ex_files == 1); % will cut out "0"s
    
    numlist = numel(filenames);
    
end


if ftype == 1
   fprintf(['\n Found ' num2str(numlist) ' individual *.h5 files.\n']);
elseif ftype == 2
   fprintf(['\n Found ' num2str(numlist) ' individual *.mat files.\n']);
elseif ftype == 3
   fprintf(['\n Found ' num2str(numlist) ' *.mat batch files.\n']);
elseif ftype == 4
   fprintf(['\n Found ' num2str(numlist) ' *.csv files.\n']);
elseif ftype == 5
   fprintf(['\n Found ' num2str(numlist) ' *.txt files.\n']);
elseif ftype == 6
   fprintf(['\n Found ' num2str(numlist) ' part* files.\n']);
end

% quit if there are no results

if numlist == 0
    fprintf('\n Terminating.\n\n');
    return
end

%% other params

fprintf('\n Initialization Thresholds:')

stable_opt = input('\n   [1] Use entire excerpt \n   [2] Set parameters manually <ENTER> \n   --> ');

if isempty(stable_opt)
    stable_opt = 2;
end

% define strings (runs and gaps) in seconds rather than events
string_type = 's';

if stable_opt == 1
    % use the entire excerpt
    local_thr = Inf;
    run_dur_thr = 0;
    gap_dur_thr = Inf;
    
elseif stable_opt == 2
    local_thr = input(['\n   Local threshold (percent difference) across successive beats and adjacent runs (<ENTER> for ' num2str(local_thr_def) '): ']);

            if isempty(local_thr)
               local_thr = local_thr_def;
            end

    run_dur_thr = input(['   Run duration threshold, in seconds (<ENTER> for ' num2str(run_dur_def) '): ']);

            if isempty(run_dur_thr)
                run_dur_thr = run_dur_def;
            end

    gap_dur_thr = input(['   Gap duration threshold, in seconds (<ENTER> for ' num2str(gap_dur_def) '): ']);

            if isempty(gap_dur_thr)
                gap_dur_thr = gap_dur_def;
            end
end

% 2013.10.04 - now we just take max value of local devations or successive changes

% fprintf('\n Percentile-based variability measures:')
% prc = input('\n   [1] [50 70 90] <ENTER>\n   [2] Other \n   --> ');
% 
% if isempty(prc)
%     prc = 1;
% end
% 
% if prc == 1
%    prc = [50 70 90];
% end
% 
% if prc == 2
%    prc = input('       Enter the desired percentiles, in [ ]: ');
% end
% 
% nprc = numel(prc);

prc = 100;

        fprintf('\n Plotting and manual trimming options:')
        plot_trim = input('\n   [1]  No plot,  No manual trim <ENTER> \n   [2] Yes plot,  No manual trim \n   [3] Yes plot, Yes manual trim \n   --> ');

        if isempty(plot_trim)
            plot_trim = 1;
        end
        
        pause_sec = 0; % just to be safe
        
        
        if plot_trim == 2 || plot_trim == 3
           pause_it = input('\n Pause between files? \n   [1] Yes <ENTER> \n   [2] No \n   --> ');
           
           if isempty(pause_it)
               pause_it = 1;
           end
           
           if pause_it == 1
               pause_sec = input('  Duration of pause, in seconds \n   [1] 1.0 second \n   [2] 3.0 seconds \n   [3] Until ENTER is pressed <ENTER> \n   --> ');
           end
           
           if isempty(pause_sec)
               pause_sec = Inf;
           end
           
           plot_color = input('\n Plot in color or black and white? \n   [1] Color <ENTER> \n   [2] Black and white \n   --> ');
           
           if isempty(plot_color)
               plot_color = 1;
           end
        end

        
% save the beats/measures data? will be useful to look at individual files
% later on, but will take up more memory and space

beats_save = input('\n Save beats and measures data for "offline" viewing? \n (Required for subsequent exploratory data analysis.)\n   [1] Yes \n   [2] No <ENTER> \n   --> ');

if isempty(beats_save)
   beats_save = 2;
end

if beats_save == 1
   % we will create the variables later
   new_name = input('\n Enter a prefix string (for ease of ID purposes): ','s');
else
   beats_all = []; % just to keep the output of BEATS consistent
   bars_all  = [];
   new_name = [];
end

% display plots?

% if input_mode == 1
%     disp_plots = input('\n Display summary plots when finished? \n   [1] Yes \n   [2] No <ENTER> \n   --> ');
% else
%     disp_plots = 2;
% end

disp_plots = 2;

% email result?
email_it =   input('\n Send email when process is finished? \n   [1] Yes \n   [2] No <ENTER> \n   --> ');

        if isempty(email_it)
            email_it = 2;
        end
        
        if email_it == 1
           send_to = input('   Target email address: ','s');
        end

        start_time = datestr(now);

% start the clock
tic

% saving the output of the beats data (accurate as of 2013.04.27)
%ncol_fin = 2 + 18 + 3*nprc; % the initial 2 is to save batch/part and individual file; we adapt for the number of percentiles (and we write out 1. percentiles, 2. PADM, 3. PASD)
ncol_fin = 2 + 14;

% 覧覧覧覧覧覧覧覧
%% Million Song DB as .h5 files
if ftype == 1 || ftype == 2

    numf = numlist;

    %msd_txt = cell(numf,4); % MSD text fields
    %msd_num =  nan(numf,11); % MSD numeric fields
   
    single_file_init; % subfunction
    
    res_final_all = nan(numf,ncol_fin+1);  % res_final_all is the correct name; +1 to add a final column which will only be when comparing Echonest data
    all_files = cell(numf,1);
    rel_start = nan(numf,1);
    rel_end   = nan(numf,1);
    
    %loc_comp = nan(numf,2);
    
    %dur_vs_spread = nan(numf,7); % we use [2 4 8 16 32 64 128] seconds
    %dur_vs_spread_min = zeros(numf,7); % nan doesn't work since we sometimes skip files?
    %dur_vs_spread_max = zeros(numf,7);
    
    % use NaNs; we can count these later on to see what is "empty" data
    artist_lat          = nan(numf,1);
    artist_loc          = cell(numf,1);
    artist_long         = nan(numf,1);
    artist_name         = cell(numf,1);
    artist_mbid         = cell(numf,1);
    album_title         = cell(numf,1);
    album_7digital_id   = cell(numf,1);
    danceability        = nan(numf,1);
    duration            = nan(numf,1);
    key                 = nan(numf,1);
    key_conf            = nan(numf,1);
    mode                = nan(numf,1);
    mode_conf           = nan(numf,1);
    tempo               = nan(numf,1);
    %tempo_conf          = nan(numf,1);
    time_sig            = nan(numf,1);
    time_sig_conf       = nan(numf,1);
    track_title         = cell(numf,1);
    track_7digital_id   = cell(numf,1);
    track_echonest_id   = cell(numf,1);
    track_echonest_path = cell(numf,1);
    year                = nan(numf,1);
   

    beats_all = cell(numf,1);

    bars_all = cell(numf,1);
    valid_check = nan(numf,2);
    
   for i = 1:numf % numf is the TOTAL number of individual files
       
       % show the current file number
       ctri = counter_indiv(i);
       fprintf(ctri); % already a string
       
       if ftype == 1
           data = HDF5_Song_File_Reader(filenames{i}); % just a single file; don't save "data"
       elseif ftype == 2
           data = readOneMillionFromMat(filenames{i});
       end
       
       
        % see http://labrosa.ee.columbia.edu/millionsong/pages/matlab-introduction
        % * indicates variables we are interested in
        %
        % HDF5_Song_File_Reader          *get_bars_start                  get_segments_loudness_start     
        % delete                         *get_beats_confidence            get_segments_pitches            
        % get_analysis_sample_rate       *get_beats_start                 get_segments_start              
        % get_artist_7digitalid          *get_danceability                get_segments_timbre             
        % get_artist_familiarity         *get_duration                    get_similar_artists             
        % get_artist_hotttnesss           get_end_of_fade_in              get_song_hotttnesss             
        % get_artist_id                   get_energy                      get_song_id                     
        %*get_artist_latitude            *get_key                         get_start_of_fade_out           
        %*get_artist_location            *get_key_confidence              get_tatums_confidence           
        %*get_artist_longitude            get_loudness                    get_tatums_start                
        %*get_artist_mbid                *get_mode                       *get_tempo                       
        % get_artist_mbtags              *get_mode_confidence            *get_time_signature              
        % get_artist_mbtags_count         get_num_songs                  *get_time_signature_confidence   
        %*get_artist_name                 get_release                    *get_title                       
        % get_artist_playmeid             get_release_7digitalid         *get_track_7digitalid            
        % get_artist_terms                get_sections_confidence        *get_track_id                    
        % get_artist_terms_freq           get_sections_start             *get_year                        
        % get_artist_terms_weight         get_segments_confidence         
        % get_audio_md5                   get_segments_loudness_max       
        %*get_bars_confidence             get_segments_loudness_max_time  

        all_files{i}        =  filenames{i}; % the long TR*** name
        
        artist_loc{i}       =  data.get_artist_location; % may be empty
        artist_lat(i)       =  data.get_artist_latitude; % may be NaN
        artist_long(i)      =  data.get_artist_longitude; % may be NaN
        artist_name{i}      =  data.get_artist_name;
        artist_mbid{i}      =  data.get_artist_mbid;
        danceability(i)     =  data.get_danceability; % 0 < X <= 1; 0 means not analyzed
        duration(i)         =  data.get_duration;
        
        key(i)              =  data.get_key; % 0 through 11
        key_conf(i)         =  data.get_key_confidence;
        mode(i)             =  data.get_mode; % 0 (minor) or 1 (major)
        mode_conf(i)        =  data.get_mode_confidence;
        
        album_title{i}      = data.get_release; % release = album
        album_7digital_id{i} = data.get_release_7digitalid; % release = album
        tempo(i)            =  data.get_tempo; % 0 means not calculated per Echonest
        %tempo_conf(i)       =  data.get_tempo_confidence;
        time_sig(i)         =  data.get_time_signature; % will be a single integer (e.g., "4" = 4/4)
        time_sig_conf(i)    =  data.get_time_signature_confidence;
        track_title{i}       =  data.get_title;
        track_7digital_id{i} =  data.get_track_7digitalid;
        track_echonest_id{i} =  data.get_track_id;
        track_echonest_path{i} = filenames{i};
        year(i)             =  data.get_year; % 0 means not reported
        
        % data to process
        beatsvar    = data.get_beats_start;
        beats_conf  = data.get_beats_confidence;

        bars        = data.get_bars_start;
        bars_conf   = data.get_bars_confidence;
        
 
        
        % ******************
        % do the BEATS calculation (subfunction)
        win = [];
        fname = data.get_title;
        
        if isempty(beatsvar) || numel(beatsvar) < 30
            % skip this file; will preserve NaNs in output
        else
            % OK
            single_file_calc % outputs "res_rows" and "res"
            
            %dur_vs_spread(i,:) = res.dur_vs_spread;
            %dur_vs_spread_min(i,:) = res.dur_vs_spread_min;
            %dur_vs_spread_max(i,:) = res.dur_vs_spread_max;
            
            % now we compare Echonest and BEATS tempo
            
            tempo_beats = res.rate_loc; % defined from single_file_calc and beats_calc
            
            tempo_echo = tempo(i); % pulled from Echonest; "0" means not evaluated
            
            % signed percentage error
            tempo_mismatch = 100 * (tempo_beats - tempo_echo) ./ tempo_echo; % will be NaN if either of the individual estimates is NaN
            
            
            % Old version 
%             if tempo_echo > 0
%                maxT  = max(tempo_beats,tempo_echo);
%                minT  = min(tempo_beats,tempo_echo);
%                signT = tempo_beats >= tempo_echo;
%                
%                if signT == 0
%                    signT = -1; % so the equation works
%                end
%                
%                tempo_mismatch = 100 * (signT * (maxT - minT)) / minT; % so we keep a single formula
%             else
%                tempo_mismatch = NaN; % cannot compare the two
%             end
            %size(res_final_all)
            %size(res_rows)
            %size(tempo_mismatch)
            res_final_all(i,:) = [nan i res_rows tempo_mismatch]; % nan because there is no batch
            rel_start(i) = res.rel_start;
            rel_end(i) = res.rel_end;
            
            valid_check(i,:) = valid_BeBa;

            
            % optionally save the beats and measures
            if beats_save == 1
               beats_all{i} = beatsvar;
               bars_all{i}  = bars;
            end
            
                

        end

        % prepare the next counter
        fprintf('\b\b\b\b\b\b\b\b')
        
   end % numf (i) loop
   
   beats_tempo = res_final_all(:,10);
   beats_meter = res_final_all(:,11);
   
   % let's also quantify beat_stability in a relative sense, from 0.5 (i.e., halfway between integers) 1.0 confidence
   
   beats_meter_rel = 0.5 + abs(0.5-mod(beats_meter,1));
   % save the tempo comparison as a single variable for easy use later
   indx = 1:numf;
   indx = indx(:);
   
   % get the minimum value for Global Temporal Stability
   gts = res_final_all(:,[14 15 16]); % based on slope, loc_transf, and suc_transf percentage values
   gts = abs(gts);
   gts = max(gts,[],2); % row max
   msd_vs_beats = [indx tempo beats_tempo time_sig beats_meter res_final_all(:,end) res_final_all(:,[5 6 7 8 9]) gts]; 
   
   % (1) Index; (2) MSD tempo; (3) BEATS rate; (4) MSD meter; (5) BEATS beats-per-bar; (6) Estimated Tempo Mismatch; 
   % (7) BEATS starting time stamp; (8) BEATS end time stamp; (9) BEATS stable duration; (10) BEATS stable percentage; 
   % (11) General Temporal Stability
  
   progressbar(1) % close it

% save the Echonest file-related info to make it easier to match up later
% on

%{

echonest_file_info fields:
1.  index (file counter)
2.  artist name
3.  artist_mbid
4.  track name
5.  echonest track ID
6.  7digital track ID
7.  album name
8.  7digital album ID
9.  duration (seconds)
10. year released
11. file path

%}

echonest_file_info = [num2cell(indx) artist_name artist_mbid track_title track_echonest_id track_7digital_id album_title album_7digital_id num2cell(duration) num2cell(year) track_echonest_path];

% we want to replace SOME fields with NaN for visual clarity (so that we
% don't "anchor" a plot with zero at the low end)
duration(duration == 0) = NaN;
danceability(danceability == 0) = NaN;
key_conf(key_conf == 0) = NaN;
mode_conf(mode_conf == 0) = NaN;
year(year == 0) = NaN;
tempo(tempo == 0) = NaN;
time_sig(time_sig == 0) = NaN;
time_sig_conf(time_sig_conf == 0) = NaN;   
   
   

if disp_plots == 1 % means we have a GUI
    
   % now a plot to compare tempo estimations for MSD vs beats.m 
   plot_eda(tempo,beats_tempo,9,201,'MSD tempo','BEATS rate'); % figure(201)
   plot_eda(time_sig,beats_meter,9,202,'MSD meter','BEATS meter'); % figure(202)
   
   % how does the spread of an IBeI series change as a function of the duration examined?
   % boxplot_rje(dur_vs_spread,[5 95],50,0)
   
%    figure(51)
%    plot(100* sum(dur_vs_spread_min)/numf,'b');
%    hold on
%    plot(100* sum(dur_vs_spread_max)/numf,'r');
%    hold off
   
%    figure(52)
%    
%    % scatter plots for PADM vs PASD
% 
%     pql10 = res_final_all(:,24);
%     pql50 = res_final_all(:,25);
%     pql90 = res_final_all(:,26);
%     
%     psq10 = res_final_all(:,27);
%     psq50 = res_final_all(:,28);
%     psq90 = res_final_all(:,29);

%     figure(150)

%         subplot(3,3,1)
%     plot(pql10,psq90,'.')
%         subplot(3,3,2)
%     plot(pql50,psq90,'.')
%         subplot(3,3,3)
%     plot(pql90,psq90,'.')
%         subplot(3,3,4)
%     plot(pql10,psq50,'.')
%         subplot(3,3,5)
%     plot(pql50,psq50,'.')
%         subplot(3,3,6)
%     plot(pql90,psq50,'.')
%         subplot(3,3,7)
%     plot(pql10,psq10,'.')
%         subplot(3,3,8)
%     plot(pql50,psq10,'.')
%         subplot(3,3,9)
%     plot(pql90,psq10,'.')
%     

end


echonest_num = [artist_lat, artist_long, danceability, duration, key, key_conf, mode, mode_conf, tempo, time_sig, time_sig_conf, year];


% then write the results (below)

% 覧覧覧覧覧覧覧覧
%% .mat files - Klapuri beats + measures (beat_batch_new.m)
elseif ftype == 3
    
numb = numel(filenames); % how many batches do we have

% how many TOTAL music files are there? (this should be pretty fast)
mf = zeros(numb,1); % will sum this
batch_list = cell(numb,1);

for b = 1:numb
    batch_name    = filenames{b};
    tem = dir(batch_name);
    
    batch_list{b} = tem.name; % just the name, not the path
    
    batch_data   = open(batch_name); 

    batch_output = batch_data.batch_output; % the Klapuri algorithm will save output as "batch output"
    mf(b) = size(batch_output.file_index,1);
    
end

k = 1; % continuous counter (across all batches)

numf = sum(mf); % actual number of files in the batches

res_final_all   = nan(numf,ncol_fin+1); % res_final_all is the correct name
all_files       = cell(numf,1);

beats_all       = cell(numf,1);
bars_all        = cell(numf,1);

rel_start       = nan(numf,1);
rel_end         = nan(numf,1);

%loc_comp        = nan(numf,2);

fprintf(['\n There are ' num2str(numf) ' total files within the ' num2str(b) ' batch files.\n'])

single_file_init % subfunction
            
% here "filenames" is the list of batch outputs
for b = 1:numb
    batch_data   = (filenames{b});
    batch_data   = open(batch_data); 
    batch_output = batch_data.batch_output; % the Klapuri algorithm will save output as "batch output"
    mfcur = size(batch_output.file_index,1);
    
        % may need to switch between double and cell structure for beats
        % and bars data. Just do this once
        
        if b == 1 
           if iscell(batch_output.beats_noncausal)
               beatstype = 1;
           else
               % double format
               beatstype = 2;
           end
           
        end
    %disp(b)   
    for i = 1:mfcur
        
       % show the current file number
       ctri = counter_indiv(k); % use k for a continuous counter
       fprintf(ctri); % already a string
        
        %disp(i)
        % extract a single file's data
        if beatstype == 1
            beatsvar = batch_output.beats_noncausal{i}; % passed to subfunction
            bars     = batch_output.meas_noncausal{i}; % passed to subfunction
        elseif beatstype == 2
            beatsvar = batch_output.beats_noncausal(i,:); % passed to subfunction
            bars     = batch_output.meas_noncausal(i,:); % passed to subfunction            
        end
        
        fpath    = batch_output.file_names{i}; % passed to subfunction
        [fpath fname] = fileparts(fpath); % fname will be the 4-digit number
        f_ind    = batch_output.file_index(i);
        
        
        beats_conf = []; % only for MSD
        bars_conf = [];  % only for MSD
        
        single_file_calc % tempo stability calculations subfunction; outputs "res_rows"
        
        %track_pts = track_pts; % leave this here
        
        tempo_mismatch = NaN; % only works for Echonest data
        res_final_all(k,:) = [b f_ind res_rows tempo_mismatch]; % b = batch index; f_ind = file index within each batch (i.e., the "actual" file index
        rel_start(k) = res.rel_start;
        rel_end(k) = res.rel_end;
        
        % location comparison
        %loc_comp(k,:) = res.loc_comp;
        
        % optionally save the beats and measures
        if beats_save == 1
           beats_all{k} = beatsvar;
           bars_all{k}  = bars;
        end
        

        
        all_files{k} = fname;

        k = k + 1; % will continue to advance to populate the final matrix
        
        % prepare the next counter
        fprintf('\b\b\b\b\b\b\b\b')
        
    end % file (i) loop

end % batch (b) loop

progressbar(1)            % Close

% then write the results (below)

% 覧覧覧覧覧覧覧覧    

%% .csv files 
elseif ftype == 4
    

    % write the results (below)
    
% 覧覧覧覧覧覧覧覧    
elseif ftype == 5
%% .txt files    
    fprintf(['\n Found ' num2str(numlist) ' files.\n']);

    % write the results (below)
    
% 覧覧覧覧覧覧覧覧    
elseif ftype == 6
%% part files
    fprintf(['\n Found ' num2str(numlist) ' part* files.\n\n Working on file: ']);
    
    % for progress bar
    p = 1; 
    prog = 10; % update 10 times
    
    % tracker for part files
    part_pts = round(linspace(1,numlist,prog)); % this will increment as part files (not individual files) are read
    part_pts = part_pts(2:end); % ignore the starting value
    
    progressbar(0,0)
    init_prog = 0; % for the readPartFile loop
    
    % read each part file
    for i = 1:numlist
        ctri = counter_indiv(i);
        fprintf(ctri) % already a string
        
        %if ismember(i,part_pts)
            %progressbar(p/prog, [])
            %p = p + 1;
        %end
       
       progressbar(i/(numlist+.01),[]) % the +.01 is just so we don't get 1/1, which closes the progressbar
        
       ind_files = readPartFile(filenames{i},init_prog); % will be a 2 column structure; this can take some time, so we insert a tracker
       nind = size(ind_files,1); % just for this particular part
       
       % save just the file names *recursively*
       
          cur_list = cell(nind,1);
          for j = 1:nind
              cur_list{j} = ['Part ' num2str(i) ', File ' num2str(j)];
          end
       

          cur_files = [cur_list ind_files(:,1)]; % just to initialize
       
       % recursive
       if i == 1
          all_files = cur_files; % just to initialize
       elseif i > 1
          all_files = [all_files; cur_files]; % individual file names
       end
       
       % ------------------
       % now processes beat data for the files in ind_files
       
       q = 1; % always reset this for the second tracker
       
       res_final = zeros(nind,ncol_fin); % just for a sub-batch
       
       if beats_save == 1
          beats_temp = cell(nind,1);
          bars_temp  = cell(nind,1); 
       end
       
       for j = 1:nind
           
           % extract the beat series
           beatsvar = ind_files(j,2);
           beatsvar = beatsvar{1}; % will be time stamp of beats now; may be empty and coded as [0]
           beats_conf = [];
           bars = []; 
           bars_conf = [];
           
           % just incase we don't have beats data
           if isempty(beatsvar) || numel(beatsvar) < 30
               % skip
           else
               % OK
               ex_inds = [];
               fname = [];

               
               [res_rows res] = beats_calc(beatsvar,beats_conf,bars,bars_conf,ex_inds,fname,plot_trim,pause_sec,ratio_thr,string_type,run_dur_thr,gap_dur_thr,prc,plot_color);
           
               % relative length
               rel_start(j) = res.rel_start;
               rel_end(j)   = res.rel_end;
               
               % location comparison
               %loc_comp(j,:) = td_out.loc_comp;
           end
                     
           % output of res is a [1 x M] vector, which we now simply pair with the file index
           tempo_mismatch = NaN; % only works for Echonest data
           res_final(j,:) = [i j res_rows tempo_mismatch]; % this will now tally part file (i) and individual file within each part (j)
           
           % optionally save the beats and measures
           if beats_save == 1
              beats_temp{j} = beatsvar(beatsvar>0); % just the real beats
              bars_temp{j} = bars;
           end
           
       end % j loop
       
       
       % now we need to write this data out recursively
       
       if i == 1
           res_final_all = res_final; % just to initialize
           
       elseif i > 1
           res_final_all = [res_final_all; res_final];
       end
       
       % do the same thing for beats and measures
       if beats_save == 1
          if i == 1
             beats_all = beats_temp;
             bars_all  = bars_temp;
          elseif i > 1
             beats_all = [beats_all; beats_temp;];
             bars_all  = [bars_all; bars_temp];
          end
       end
       
        % prepare the next counter
        fprintf('\b\b\b\b\b\b\b\b')
        
    end % i loop
    progressbar(1) % close it
    
% write the results (below)
   
      
end % ftype selection


%% subfunction: single_file_init

    function single_file_init % output: res_final
        
        % for progress bar
        %p = 1; 
        %prog = 101; % every percentage point
        %track_pts = floor(linspace(1,numf,prog));
        %track_pts = unique(track_pts(2:end)); % ignore the starting value
        
        res_final = nan(numf,ncol_fin); % will preserve NaNs if file is not read

        fprintf('\n Working on file: ');
        
        % visualize a tracker
        progressbar(0)  % Initialize progress bar
        p = 1; % starting counter
    end

%% subfunction: single_file_calc

    function single_file_calc % writes results to res_final
        % this runs INSIDE the numf loop       
        
            %if ismember(i,track_pts)     
                progressbar(p/numf)
                p = p + 1;
            %end

            % calculations
            ex_inds = [];
            
            [res_rows res] = beats_calc(beatsvar,beats_conf,bars,bars_conf,ex_inds,fname,plot_trim,pause_sec,local_thr,string_type,run_dur_thr,gap_dur_thr,prc,plot_color);

            % get rid of zeros or NaNs
            beatsvar = beatsvar(beatsvar > 0);
            bars = bars(bars > 0);
            
            valid_BeBa = nan(1,2);
            valid_BeBa(1) = numel(beatsvar) > 0;
            valid_BeBa(2) = numel(bars) > 0;

            % the next steps will be ftype specific
    end


%% final output

% calculate which run method was used
% stat =  1 means  1 > (2 or 3)
% stat =  2 means  2 > (1 or 3)
% stat =  3 means  3 > (1 or 2)
% stat =  4 means (1 = 2) > 3
% stat =  5 means (2 = 3) > 1
% stat =  6 means (1 = 3) > 2
% stat =  7 means (1 = 2 = 3)
% stat = 99 means no winner

% run_meth = res_final_all(:,9);
% 
% run1  = sum(run_meth == 1)  / numel(run_meth) * 100; 
% run2  = sum(run_meth == 2)  / numel(run_meth) * 100;
% run3  = sum(run_meth == 3)  / numel(run_meth) * 100; 
% run4  = sum(run_meth == 4)  / numel(run_meth) * 100; 
% run5  = sum(run_meth == 5)  / numel(run_meth) * 100; 
% run6  = sum(run_meth == 6)  / numel(run_meth) * 100; 
% run7  = sum(run_meth == 7)  / numel(run_meth) * 100; 
% run99 = sum(run_meth == 99) / numel(run_meth) * 100; 

% stable length summary
stab_dur = res_final_all(:,7);

%stab_prc = prctile_nist(stab_dur,[50 75 90]);

% for batch outputs
if ftype == 3 
    output.batch_list = batch_list;
    
elseif ftype == 6
      
end 

    % ---------------------------------------
    % for all outputs
    main.defaults.local_thr = local_thr;
    main.defaults.run_dur_thr = run_dur_thr;
    main.defaults.gap_dur_thr = gap_dur_thr;
    main.defaults.percentiles = prc;
    xtra.file_list = all_files; % individual file names

    xtra.beats_all = beats_all; % will be [] if beats_save == 2
    xtra.bars_all = bars_all; % will be [] if beats_save == 2
    %xtra.stable_dur.p50 = stab_prc(1);
    %xtra.stable_dur.p75 = stab_prc(2);
    %xtra.stable_dur.p90 = stab_prc(3);
   
%     xtra.run_method.median_best= run1;
%     xtra.run_method.mode_best  = run2;
%     xtra.run_method.cstat_best = run3;
%     xtra.run_method.me_tie_mo  = run4;
%     xtra.run_method.mo_tie_cs  = run5;
%     xtra.run_method.me_tie_cs  = run6;
%     xtra.run_method.all_tie    = run7;
%     xtra.run_method.none_valid = run99;
    
    
    % separate file
    main.all_results   = res_final_all; % [N rows by M columns]
    main.process_time = toc; % in seconds
    
if ftype == 2
    xtra.echonest_num = echonest_num; %all of the echonest fields
    main.msd_vs_beats = msd_vs_beats; % 12 fields
    main.echonest_file_info = echonest_file_info;
    main.valid_check = valid_check; % col1 = valid beats; col2 = valid bars (1 means valid)
    
end


% description of columns
fprintf(['Finished. \n\n The column structure of "all_results" is as follows:',...
         '\n   1. Batch or part number (if present, otherwise "NaN")',...
         '\n   2. File number / File ID',...
         '\n 3-4. Start and Stop indices',...
         '\n 5-6. Start and Stop times',...
         '\n   7. Stable duration (s)',...
         '\n   8. Stable percentage (of total file)',...
         '\n   9. Run percentage (percentage of Stable Segment occupied by Runs)',...
         '\n  10. Rate, based on central tendency of full IBeI series (bpm)',...
         '\n  11. Meter estimation',...
         '\n  12. Beats confidence (percentile)',...
         '\n  13. Bars confidence (percentile)',...
         '\n  14. Delta slope (signed percentage change from beginning to end)',...
         '\n  15. Max. of percentage deviations from location (across Runs)',...
         '\n  16. Max. of successive percentage changes (across Runs)',...
         '\n  17. Tempo Error: signed percentage error between BEATS and Echonest estimates of tempo (NaN otherwise)',...
         '\n\n'])

     fprintf([ ' Column structure of echonest_file_info is as follows:',...
         '\n   1.  Index (file counter)',...
         '\n   2.  Artist name',...
         '\n   3.  Artist_mbid',...
         '\n   4.  Track name',...
         '\n   5.  Echonest track ID',...
         '\n   6.  7digital track ID',...
         '\n   7.  Album name',...
         '\n   8.  7digital album ID',...
         '\n   9.  Duration (seconds)',...
         '\n  10. Year released',...
         '\n  11. File path',...
         '\n\n'])


% ************************************
% PLOTS
if disp_plots == 1 % means we have a GUI

    % where within a file are most songs stable?

    if ftype == 1 || ftype == 2 || ftype == 3 % haven't done this for ftype == 6 yet

       nfile = size(rel_start,1); % how many rows
       xvals = 0:1:100; % this will give us 101 bins

       all_f = zeros(nfile,101);

       % calculation 
       for n = 1:nfile
           
           if (rel_start(n) >= 0) && (rel_end(n) > rel_start(n))
               % ok
               all_f(n,(rel_start(n)+1 : rel_end(n)+1)) = 1; % turn it to 1
           else
               % stay at 0
           end
       end
  
       %all_f = all_f(:,2:end);
       %xvals = xvals(2:end);
       
       % now make the plot
       figure(250)
       plot(xvals,(sum(all_f)/nfile*100),'b')
       xlabel('Time index deemed stable (relative)')
       ylabel('Percentage of total files')
       
       % a different plot
       rel_dur = rel_end - rel_start; % total length
       rel_all = [rel_start rel_end rel_dur]; %columns
       
       % sort it
       rel_all = sortrows(rel_all,[1 3]); % sort by duration, then by start
       
       do_this = 0;
       if do_this == 1
           figure(255)
           for n = 1:nfile
               if (rel_start(n) >= 0) && (rel_end(n) > rel_start(n))
                   plot([rel_all(n,1) rel_all(n,2)],[n n],'b')
                   hold on
               else
                   % don't plot
               end
           end
           hold off
       end
    end

    % figure(400)
    % location comparison
    % plot(loc_comp(:,1),loc_comp(:,2),'.')
    
end

%% save and email results


save_date = datestr(now,29); % the date as YYYY-MM-DD
save_time = datestr(now,13); % the time
save_time = [save_time(1:2) '.' save_time(4:5) '.' save_time(7:8)]; % so it will be a saveable name
save_dt = [save_date '_' save_time];

if isempty(new_name)
   spacer = []; 
else
   spacer = '_'; 
end

% save the [N x 29] structure (small)
save2_inst = ['save ' new_name spacer 'BEATS_main_' save_dt '.mat main'];
save2_name = [        new_name spacer 'BEATS_main_' save_dt '.mat'];

% save the rest of the data (large)
save1_inst = ['save ' new_name spacer 'BEATS_xtra_' save_dt '.mat xtra'];
save1_name = [        new_name spacer 'BEATS_xtra_' save_dt '.mat'];

if beats_save == 1
    eval(save1_inst)  % very simple
    eval(save2_inst) 
    fprintf(['\n Two .mat output files was written to the working directory (pwd): \n    <' save2_name '> \n    <' save1_name '> \n\n']);
end

% now send this as an email, if specified to
if email_it == 1
   email_me(save2_name,send_to,start_time,[]); 
   % note: the files are just too large to send via email
end



end % beats function