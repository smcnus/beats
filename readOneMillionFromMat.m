function result = readOneMillionFromMat(fileName)

% wrapper to extract Million Song Dataset fields from *.h5 files that have
% been converted to *.mat. This was done because MATLAB would crash when it
% encountered a MSD file that had a blank field for beats, when directly
% reading from *.h5
%
% John CAI | 2013.03.09

% 'result' contains all the field in a song
% For example, result.get_title returns the title of the song
    
         
        % get_analysis_sample_rate       *get_beats_start                 get_segments_start              
        % get_artist_7digitalid          *get_danceability                get_segments_timbre             
        % get_artist_familiarity         *get_duration                    get_similar_artists             
        % get_artist_hotttnesss           get_end_of_fade_in              get_song_hotttnesss             
        % get_artist_id                   get_energy                      get_song_id                     
        %*get_artist_latitude            *get_key                         get_start_of_fade_out           
        %*get_artist_location            *get_key_confidence              get_tatums_confidence           
        %*get_artist_longitude            get_loudness                    get_tatums_start                
        % get_artist_mbid                *get_mode                       *get_tempo                       
        % get_artist_mbtags              *get_mode_confidence            *get_time_signature              
        % get_artist_mbtags_count         get_num_songs                  *get_time_signature_confidence   
        %*get_artist_name                 get_release                    *get_title                       
        % get_artist_playmeid             get_release_7digitalid         *get_track_7digitalid            
        % get_artist_terms                get_sections_confidence         get_track_id                    
        % get_artist_terms_freq           get_sections_start             *get_year                        
        % get_artist_terms_weight         get_segments_confidence         
        % get_audio_md5                   get_segments_loudness_max       
        %*get_bars_confidence             get_segments_loudness_max_time  
    mode = 1000;
    title = '';
    year = 0;
    load(fileName);
    result.get_analysis_sample_rate = analysis_sample_rate; 
    result.get_artist_7digitalid = artist_7digitalid; 
    result.get_artist_familiarity = artist_familiarity;
    result.get_artist_hotttnesss = artist_hotttnesss;
    result.get_artist_id = artist_id; 
    result.get_artist_latitude = artist_latitude; 
    result.get_artist_location=artist_location;
    result.get_artist_longitude=artist_longitude;
    result.get_artist_mbid=artist_mbid;
    result.get_artist_mbtags=artist_mbtags;
    result.get_artist_mbtags_count=artist_mbtags_count;
    result.get_artist_name=artist_name;
    result.get_artist_playmeid=artist_playmeid;
    result.get_artist_terms=artist_terms;
    result.get_artist_terms_freq=artist_terms_freq;
    result.get_artist_terms_weight=artist_terms_weight;
    result.get_audio_md5=audio_md5;
    result.get_bars_confidence=bars_confidence;
    result.get_bars_start=bars_start;
    result.get_beats_confidence=beats_confidence;
    result.get_beats_start=beats_start;
    result.get_danceability=danceability;
    result.get_duration=duration;
    result.get_end_of_fade_in=end_of_fade_in;
    result.get_energy= energy;
    result.get_key=key;
    result.get_key_confidence=key_confidence;
    result.get_loudness=loudness;
    result.get_mode = mode;
    result.get_mode_confidence=mode_confidence;
    result.get_release=release;
    result.get_release_7digitalid=release_7digitalid;
    result.get_sections_confidence=sections_confidence;
    result.get_sections_start=sections_start;
    result.get_segments_confidence=segments_confidence;
    result.get_segments_loudness_max=segments_loudness_max;
    result.get_segments_loudness_max_time=segments_loudness_max_time;
    result.get_segments_loudness_start=segments_loudness_start;
    result.get_segments_pitches=segments_pitches;
    result.get_segments_start=segments_start;
    result.get_segments_timbre=segments_timbre;
    result.get_similar_artists=similar_artists;
    result.get_song_hotttnesss=song_hotttnesss;
    result.get_song_id=song_id;
    result.get_start_of_fade_out=start_of_fade_out;
    result.get_tatums_confidence= tatums_confidence;
    result.get_tatums_start=tatums_start;
    result.get_tempo=tempo;
    result.get_time_signature=time_signature;
    result.get_time_signature_confidence=time_signature_confidence;
    result.get_title = title;
    result.get_track_7digitalid = track_7digitalid;
    result.get_track_id = track_id;
    result.get_transfer_note = transfer_note; 
    result.get_year = year;