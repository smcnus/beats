function output = msd_data

% returns data from all unique ARTISTS
%
% RJ Ellis 
% 2013.10.15


%% hello

loc = which('beats'); % note: must have a unique name and NOT an internal variable name
                      % in order to have the function called "beats", the
                      % internal variable must have a different name; i.e.,
                      % "beatsvar"
file_info = dir(loc);
save_date = file_info.date;

fprintf(['\n\n || BEATS: Basic Exploration of Auditory Temporal Stability \n || Version: ' save_date '\n || For more information, see: \n ||   http://m3r.comp.nus.edu.sg/wiki/index.php/Main_Page \n ||   http://robjellis.net \n']);


% check to see if we have robustfit toolbox in matlab (or other important
% functions?)

checkrob = which('robustfit'); % make sure we have it

if sum(size(checkrob)) > 0
   % OK
else
    fprintf('\n Warning: Could not find MATLAB "robustfit" function.\n\n')
end
    
%% identify file type 

ftype = 2;

% fprintf('\n To-be-processed file type:')
% ftype = input(['\n   [1] Echonest / Million Song DB (individual *.h5 files)',...
%                '\n   [2] Echonest / Million Song DB (individual *.mat files)',...
%                '\n   [3] Klapuri beats + bars (*.mat batch files)',...
%                '\n   [4] *.csv',... 
%                '\n   [5] *.txt',... 
%                '\n   [6] XiaMi DB (part* batch files)',...
%                '\n   --> ']);

if ftype == 1
   tmatch = '.h5';
   datamsg = 'Select the parent directory of the *.h5 individual files';
elseif ftype == 2
   tmatch = '.mat';
   datamsg = 'Select the parent directory of the *.mat individual files';
elseif ftype == 3
   tmatch = '.mat';
   datamsg = 'Select the parent directory of the *.mat batch output files';
elseif ftype == 4
   tmatch = '.csv';
   datamsg = 'Select the parent directory of the *.csv files';
elseif ftype == 5
   tmatch = '.txt';
   datamsg = 'Select the parent directory of the *.txt files';
elseif ftype == 6
   tmatch = 'part';
   datamsg = 'Select the parent directory of the part* files';
elseif isempty(ftype)
   fprintf('\n\n Error: No file type selected. \n\n')
   return
end


%% select the target directory

input_mode = input('\n Input mode: \n   [1] GUI <ENTER> or \n   [2] Command line \n   --> ');

if isempty(input_mode)
   input_mode = 1;
end

if input_mode == 1
    inpath = uigetdir(pwd,datamsg);
    disp_plot = 1; % we have a GUI, so we can display plots
elseif input_mode == 2
    inpath = input('   Enter the full path (no quotes): ','s');
    addpath(inpath)
    disp_plot = 0;
end

% check to see if the last character is a filesep
if strcmp(inpath(end),filesep)
   % OK
else
   inpath = [inpath filesep]; % add filesep
end

cd(inpath); % this will be the output data directory


%% tally the files

fprintf(' \n Finding subdirectories ...');

% the following should work for ALL data types
showprog = 1; % display the ticker on screen

filenames = findAllFiles_rje(inpath,tmatch,showprog); % code by Dan Ellis + rje edit (to match any part of the file name)
fprintf('\n') % just leave this

numlist = numel(filenames); % this will work for datasets where numlist indicates *single* files, but will be redefined for batches


% a couple of special cases to deal with
if ftype == 2 % exclude .mat outputs from BEATS; all MSD songs start with TR****.mat
    ex_files = ones(numlist,1);
        
        for i = 1:numlist
            [pathname filename] = fileparts(filenames{i});
            if sum(findstr(filename,'BEATS')) > 0 % will match the string "BEATS" in caps
                ex_files(i) = 0; % move to zero
            elseif sum(findstr(filename,'ShowingContentIndex')) > 0 
                ex_files(i) = 0; % move to zero
            else
                % stay at one
            end

        end

    filenames = filenames(ex_files == 1); % will cut out "0"s
    numlist = numel(filenames);   
 
elseif ftype == 3 % Only include "klapuri_batch_output*.mat" files
    ex_files = ones(numlist,1);

        for i = 1:numlist
            [pathname filename] = fileparts(filenames{i});
            if sum(findstr(filename,'klapuri_batch_output')) > 0
                % stay at one
                
            else
                % exclude this case
                ex_files(i) = 0;
            end

        end

    filenames = filenames(ex_files == 1); % will cut out "0"s
    
    numlist = numel(filenames);
    
end


if ftype == 1
   fprintf(['\n Found ' num2str(numlist) ' individual *.h5 files.\n']);
elseif ftype == 2
   fprintf(['\n Found ' num2str(numlist) ' individual *.mat files.\n']);
elseif ftype == 3
   fprintf(['\n Found ' num2str(numlist) ' *.mat batch files.\n']);
elseif ftype == 4
   fprintf(['\n Found ' num2str(numlist) ' *.csv files.\n']);
elseif ftype == 5
   fprintf(['\n Found ' num2str(numlist) ' *.txt files.\n']);
elseif ftype == 6
   fprintf(['\n Found ' num2str(numlist) ' part* files.\n']);
end

% quit if there are no results

if numlist == 0
    fprintf('\n Terminating.\n\n');
    return
end


% give it a name
%new_name = input('\n Enter a prefix string (for ease of ID purposes): ','s');

new_name = [];

% email result?
email_it =   input('\n Send email when process is finished? \n   [1] Yes \n   [2] No <ENTER> \n   --> ');

        if isempty(email_it)
            email_it = 2;
        end
        
        if email_it == 1
           send_to = input('   Target email address: ','s');
        end

        start_time = datestr(now);

% start the clock
tic


% 覧覧覧覧覧覧覧覧
%% Million Song DB as .h5 files
if ftype == 1 || ftype == 2

    numf = numlist;
   
   
    
    % use NaNs; we can count these later on to see what is "empty" data
    all_files           = cell(numf,1);
    artist_lat          = nan(numf,1);
    artist_loc          = cell(numf,1);
    artist_long         = nan(numf,1);
    artist_name         = cell(numf,1);
    artist_7digital_id  = nan(numf,1);
    artist_mbid         = cell(numf,1);
    artist_terms        = cell(numf,100);
    artist_terms_top10    = cell(numf,10); % just top 10
    artist_terms_freq   = nan(numf,100);
    artist_terms_weight = nan(numf,100);
    
    min_w_5 = zeros(numf,1);
    min_w_10 = zeros(numf,1);
    num_terms = zeros(numf,1); 
    
    % storing freq and weight
    min_fw_p50 = zeros(numf,1);
    min_fw_p75 = zeros(numf,1);
    
    album_title         = cell(numf,1);
    album_7digital_id   = nan(numf,1);

    track_title         = cell(numf,1);
    track_7digital_id   = nan(numf,1);
    track_echonest_id   = cell(numf,1);
    track_echonest_path = cell(numf,1);
    year                = nan(numf,1);
   
    
   for i = 1:numf % numf is the TOTAL number of individual files
       
       % show the current file number
       ctri = counter_indiv(i);
       fprintf(ctri); % already a string
       
       if ftype == 1
           data = HDF5_Song_File_Reader(filenames{i}); % just a single file; don't save "data"
       elseif ftype == 2
           data = readOneMillionFromMat(filenames{i});
       end
       
       
        % see http://labrosa.ee.columbia.edu/millionsong/pages/matlab-introduction
        % * indicates variables we are interested in
        %
        % HDF5_Song_File_Reader          *get_bars_start                  get_segments_loudness_start     
        % delete                         *get_beats_confidence            get_segments_pitches            
        % get_analysis_sample_rate       *get_beats_start                 get_segments_start              
        % get_artist_7digitalid          *get_danceability                get_segments_timbre             
        % get_artist_familiarity         *get_duration                    get_similar_artists             
        % get_artist_hotttnesss           get_end_of_fade_in              get_song_hotttnesss             
        % get_artist_id                   get_energy                      get_song_id                     
        %*get_artist_latitude            *get_key                         get_start_of_fade_out           
        %*get_artist_location            *get_key_confidence              get_tatums_confidence           
        %*get_artist_longitude            get_loudness                    get_tatums_start                
        %*get_artist_mbid                *get_mode                       *get_tempo                       
        % get_artist_mbtags              *get_mode_confidence            *get_time_signature              
        % get_artist_mbtags_count         get_num_songs                  *get_time_signature_confidence   
        %*get_artist_name                 get_release                    *get_title                       
        % get_artist_playmeid             get_release_7digitalid         *get_track_7digitalid            
        % get_artist_terms                get_sections_confidence        *get_track_id                    
        % get_artist_terms_freq           get_sections_start             *get_year                        
        % get_artist_terms_weight         get_segments_confidence         
        % get_audio_md5                   get_segments_loudness_max       
        %*get_bars_confidence             get_segments_loudness_max_time  

        all_files{i}        =  filenames{i}; % the long TR*** name
        
        artist_loc{i}       =  data.get_artist_location; % may be empty
        artist_lat(i)       =  data.get_artist_latitude; % may be NaN
        artist_long(i)      =  data.get_artist_longitude; % may be NaN
        artist_name{i}      =  data.get_artist_name;
        artist_7digital_id(i) = data.get_artist_7digitalid;
        artist_mbid{i}      =  data.get_artist_mbid;
        
        xx = cellstr(data.get_artist_terms);
        yy = data.get_artist_terms_freq;
        zz = data.get_artist_terms_weight;
        
        xx = xx'; % row vector
        nx = size(xx,2);
        
        if nx > 1
            artist_terms(i,1:nx) = xx;
            num_terms(i) = numel(xx);
            
            % cut terms with weight < 0.5
            terms_cut = xx(zz >= 0.5);

            if numel(terms_cut) > 10
                artist_terms_top10(i,1:10) = terms_cut(1:10);
            else
                artist_terms_top10(i,1:numel(terms_cut)) = terms_cut;
            end
            
            ny = size(yy,1);
            artist_terms_freq(i,1:ny)   = yy';                      
            
            zz = zz(:);
            nz = size(zz,1);
            artist_terms_weight(i,1:nz) = zz';
            if nx >= 5
                min_w_5(i) = min(zz(1:5));
            else
                % stay at 0
            end
            
            if nx >= 10
                min_w_10(i) = min(zz(1:10));
            else
                % stay at 0
            end
            
            % minimum value across freq and weight
            min_fw = min(yy,zz);
            
            % note: method below will correctly tally "0"s
            min_fw_p50(i) = sum(min_fw >= 0.5);
            min_fw_p75(i) = sum(min_fw >= 0.75);
        else
            %artist_terms(i,1) = NaN;
            %artist_terms_freq(i,1) = NaN;
            %artist_terms_weight(i,1) = NaN;
        end
        
        album_title{i}      = data.get_release; % release = album
        album_7digital_id(i) = data.get_release_7digitalid; % release = album

        track_title{i}       =  data.get_title;
        track_7digital_id(i) =  data.get_track_7digitalid;
        track_echonest_id{i} =  data.get_track_id;
        track_echonest_path{i} = filenames{i};
        year(i)             =  data.get_year; % 0 means not reported
             

        % prepare the next counter
        fprintf('\b\b\b\b\b\b\b\b')
        
   end % numf (i) loop
   
   progressbar(1) % close it

   
end % ftype selection

%% remove artist duplicates

% how many missing
num_missing = sum(artist_7digital_id == 0);

[un_id un_ind] = unique(artist_7digital_id);

artist_name = artist_name(un_ind);
artist_7digital_id = artist_7digital_id(un_ind);
artist_mbid = artist_mbid(un_ind);
artist_terms = artist_terms(un_ind,:);
artist_terms_top10 = artist_terms_top10(un_ind,:);
artist_terms_freq = artist_terms_freq(un_ind,:);
artist_terms_weight = artist_terms_weight(un_ind,:);

%% plots

freq_all = artist_terms_freq(:);
weight_all = artist_terms_weight(:);

incl1 = freq_all > 0; % get rid of NaN
incl2 = weight_all > 0;
incl = min(incl1,incl2); % will work by rows

% now we make them the same number of values
freq_all = freq_all(incl); 
weight_all = weight_all(incl);
corr_fw = corr(freq_all,weight_all);
num_fw_pairs = sum(incl);

% ECDF
x = 0:1:100; x = x(:);
y_w_5 = prctile_nist(min_w_5,x);
y_w_10 = prctile_nist(min_w_10,x);

if disp_plot == 1
    
    figure(400)
    plot(freq_all,weight_all,'.')

    
    figure(500)
    
    % number of terms
    subplot(1,4,1)
    ecdf(num_terms)
    title('Number of Terms')
    ylabel('CDF')
    
    % histogram of freq
    subplot(1,4,2)
    plot(y_w_5, x)
    title('Min 5th place Weight')
    ylabel('Percentile')
    xlabel('Weight')
    
    % histogram of weight
    subplot(1,4,3)
    plot(y_w_10, x)
    title('Min 10th place Weight')
    ylabel('Percentile')
    xlabel('Weight')
    
    % min of freq. and weight
    subplot(1,4,4)
    ecdf(min_fw_p50) % tally = 0 will be plotted if present
    title('Min. Freq Weight > 0.50')
    ylabel('CDF')
   
end

%% outputs
output.num_files = numlist;
output.num_missing_IDs = num_missing;
output.artist_name = artist_name;
output.artist_7digital_id = artist_7digital_id;
output.artist_mbid = artist_mbid;
%output.artist_terms = artist_terms;
output.artist_terms_top10 = artist_terms_top10;
%output.artist_terms_freq = artist_terms_freq;
%output.artist_terms_weight = artist_terms_weight;
output.corr_fw = corr_fw;
output.num_fw_pairs = num_fw_pairs;
output.ecdf_w_5 = [y_w_5 x];
output.ecdf_w_10 = [y_w_10 x];



% 
% %% save and email results
% 
% save_date = datestr(now,29); % the date as YYYY-MM-DD
% save_time = datestr(now,13); % the time
% save_time = [save_time(1:2) '.' save_time(4:5) '.' save_time(7:8)]; % so it will be a saveable name
% save_dt = [save_date '_' save_time];
% 
% if isempty(new_name)
%    spacer = []; 
% else
%    spacer = '_'; 
% end
% 
% % save the [N x 29] structure (small)
% save2_inst = ['save ' new_name spacer 'BEATS_main_' save_dt '.mat main'];
% save2_name = [        new_name spacer 'BEATS_main_' save_dt '.mat'];
% 
% % save the rest of the data (large)
% save1_inst = ['save ' new_name spacer 'BEATS_xtra_' save_dt '.mat xtra'];
% save1_name = [        new_name spacer 'BEATS_xtra_' save_dt '.mat'];
% 
% eval(save1_inst)  % that simple!
% eval(save2_inst)  % that simple!
% 

% now send this as an email, if specified to
if email_it == 1
   email_me('msd_data',send_to,start_time,[]); 
   % note: the files are just too large to send via email
end
% 
% fprintf(['\n Two .mat output files was written to the working directory (pwd): \n    <' save2_name '> \n    <' save1_name '> \n\n']);

end % beats function