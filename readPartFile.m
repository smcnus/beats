function [output] = readPartFile(fileName,init_prog)
% function readPartFile() is to read the metadata with the name including 'part'. 
% If there is no data for a given song, this is coded as "0" in the data field.
% 
% 2013/3/4 By John + additions by rje 2013.03.05


file = fopen(fileName);

if(file == -1)
    % The file do not exist.
    assertion = sprintf('The file "%s" does not exist',fileName);
    assert(exist(fileName,'file')~=0, assertion);
    return;
end

if init_prog == 0   
    % the progressbar is already be initialized 
else
    progressbar(0,0)
end

wholeText = fscanf(file,'%c');%The whole test file is stored in ONE string
fclose(file);

dividedToLine = regexp(wholeText,'\n','split');
numOfLine = size(dividedToLine,2);

output = cell(numOfLine,2);%The output result

% tracker for indiv files
p = 1;
prog = 10; % 10 divisions
indiv_pts = round(linspace(1,numOfLine-1,prog));
indiv_pts = indiv_pts(2:end);
           
for i = 1 : numOfLine-1
    
    %progress = progress + progressUnit;
    %progressbar(progress);
    
           % update the tracker
           %if ismember(i,indiv_pts)
                %progressbar([], p/prog)
                %p = p + 1;
           %end
           progressbar([],i/(numOfLine+.01)) % the +.01 is just so we don't get 1/1, which closes the progressbar
           
    twoPart = regexp(char(dividedToLine{i}),'\t','split');
    
    name = char(twoPart{1});
    data = char(twoPart{2});
    if(strcmp(data,'None'))
        output{i,1} = name;
        output{i,2} = 0;
        continue;
    end
    splitedData = regexp(data,',','split');
    numOfPoints = size(splitedData,2);
    clear dataArray;
    dataArray = zeros(1,numOfPoints);
    for j = 1 : numOfPoints
        dataArray(j) = str2double(char(splitedData{j}));
    end
    output{i,1} = name;
    output{i,2} = dataArray;
end
%progressbar(1);

% save the file (can comment out)
%saveFileName = sprintf('output_%s',fileName);
%save(saveFileName,'output');

