function [output] = beats_count(data, col, thr, absval, sign)

% easy way to count cases that satisfy certain conditions
% parameters can be vectors, and are done in sequence
%
% [num_cases] = msd_count(data, col, thr, sign, absval)
%
% "sign" 
%   =  1 means incl >= thr
%   = -1 means incl <= thr
%   =  0 means incl == thr
%
% "absval"
%   =  1 means take abs val of data
%   =  0 means don't

 

if nargin < 2
  nparam = 0;
else
    
   nparam = numel(col);

    % make sure number of parameters is correct

    if numel(col) == numel(thr) && numel(thr) == numel(sign) && numel(sign) == numel(absval)
       %OK
    else
        fprintf('\n\n Error: not all parameters have equal number of cases.\n\n');
        return
    end

end
% FIRST, we want to get quantiles from the FULL dataset

% use 10, 50, or 90 percentile?


%padm = data(:,[24 25 26]); % all three for the later loop
%pasd = data(:,[27 28 29]);


% log transformation - optional

%padm = log(padm+1);
%pasd = log(pasd+1);




% =============================================
% now we reduce based on the other inclusion criteria

if nparam == 0
    % do nothing; data in = data out
else
    for i = 1:nparam
       incl = data(:,col(i));

       if absval(i) == 1
          incl = abs(incl);
       end

       if sign(i) == 1
          incl = incl >= thr(i);
       elseif sign(i) == -1
          incl = incl <= thr(i); 
       elseif sign(i) == 0
          incl = incl == thr(i); % for meter, e.g., we need to do this three times: col = [12 12 12]; thr = [2 4 8];, etc.
       end


       data = data(incl,:); % will get smaller and smaller 

    end
end
% finally we count the number of rows

output.num_cases = size(data,1);

%% THE NEW DATA

% after we filter out cases we don't want, then get quantiles for PADM and PASD

%% stable duration calculations
sd_x = 60:20:240; sd_x = sd_x(:);
sd = data(:,7);
total_files = size(data,1);
  
sd = sd(sd >= 0); % cut out NaN

sd_y = zeros(numel(sd_x),1);

for i = 1:numel(sd_x);
    sd_y(i) = sum(sd >= sd_x(i));
end

sd_y = 100* sd_y / total_files;


if size(data,2) > 25
    padm = data(:,[24 25 26]); % all three for the later loop
    pasd = data(:,[27 28 29]);

    % log transformation

    %padm = log(padm+1);
    %pasd = log(pasd+1);

    padm10 = padm(:,1);
    padm50 = padm(:,2);
    padm90 = padm(:,3);

    pasd10 = pasd(:,1);
    pasd50 = pasd(:,2);
    pasd90 = pasd(:,3);

    stats.num_cases = size(data,1);

    % quant = 10;
    % 
    % prc = linspace(0,100,quant+1);
    % prc = prc(2:end-1); % we only care about the middle values
    % 
    % padm_prc(1,:) = prctile_nist(padm10,prc);
    % padm_prc(2,:) = prctile_nist(padm50,prc);
    % padm_prc(3,:) = prctile_nist(padm90,prc);
    % 
    % pasd_prc(1,:) = prctile_nist(pasd10,prc);
    % pasd_prc(2,:) = prctile_nist(pasd50,prc);
    % pasd_prc(3,:) = prctile_nist(pasd90,prc);


    output.stats = stats;


  
    
  %% plotting
  % only do plots if we have data to plot
  
  if size(data,1) > 10
        % 2D plot of PADM vs PASD, with the quantiles highlighted
        % also need to filter out based on the other conditions

        for p = 1:3 % just plot padm90 now

            x = padm(:,p);
            y = pasd(:,p);

            % now do the quantile tag
            padm_tags = quantile_tag(x,10);
            pasd_tags = quantile_tag(y,10);


            qx = padm_tags.quantiles;
            qy = pasd_tags.quantiles;

            nquant = numel(qx);
            min_x = min(x);
            max_x = max(x);
            min_y = min(y);
            max_y = max(y);

            plot_eda(x,y,4,300+p,'padm','pasd'); % this will give us the interactive data cursor
            hold on

            for q = 1:nquant
                % padm quantiles
                plot([qx(q) qx(q)],[min_y max_y],'m');


                % pasd quantiles
                plot([min_x max_x],[qy(q) qy(q)],'m');
            end
            hold off
        end

         % next, a new statistic
    tot_var = log(x+1)+log(y+1); % in log units

    % add this to the data output
    data(:,31) = tot_var;

    
    tot_var_top = prctile_nist(tot_var,95);

    tot_var_top_ind = tot_var > tot_var_top; % binarized

    top_padm90 = padm90(tot_var_top_ind == 1);
    top_pasd90 = pasd90(tot_var_top_ind == 1);
    
        figure(303)
        hold on
        %plot(top_padm90,top_pasd90,'g*')
        hold off

        figure(304)
        plot(sort(tot_var))

        % also show the ETM plot
        figure(350)
        plot(data(:,10),data(:,30),'*')
  else
      % not enough data
      fprintf('\n Not enough data to display plots.\n\n')
  end
end

%% outputs

output.data_out = data; % the NEW data
output.index_out = data(:,2); % 2nd column is the retained indices (to cross-reference with file names, etc)
output.sd_x = sd_x;
output.sd_y = sd_y;