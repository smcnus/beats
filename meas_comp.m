function meas_comp

% facilitates comparing output of beat detection data from Echonest vs.
% Klapuri algorithms, processed by BEATS

%% input data

[f p] = uigetfile(pwd,'Select the Klapuri file');
kfile = [p f];

[f p] = uigetfile(pwd,'Select the Echonest file');
efile = [p f];

kdata = load(kfile);
kdata = kdata.output;

edata = load(efile);
edata = edata.output;

% confirm same file number

nk = numel(kdata.file_list);
ne = numel(edata.file_list);

    if nk == ne
        % OK
    else
        fprintf(['\n Error: Counts of Klapuri (' num2str(nk) ' files) and Echonest (' num2str(ne) ' files) do not agree.\n\n'])
        return
    end

%% read in data
kcols = kdata.results;
ecols = edata.results;

%% plots

% length = col
col = 7;
name = 'Length (s)';
plot_eda(ecols(:,col),kcols(:,col),9,101,name);

% stable percent = col 8
col = 8;
name = 'Stable %';
plot_eda(ecols(:,col),kcols(:,col),9,102,name);

% median rate = col 9
col = 9;
name = 'Rate (md)';
plot_eda(ecols(:,col),kcols(:,col),9,103,name);

% meter = col 11
col = 11;
name = 'Meter';
plot_eda(ecols(:,col),kcols(:,col),9,104,name);

% padm = col 23
col = 23;
name = 'PADM90';
plot_eda(ecols(:,col),kcols(:,col),9,105,name);

% pasd = col 26
col = 26;
name = 'PASD90';
plot_eda(ecols(:,col),kcols(:,col),9,106,name);