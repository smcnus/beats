function [output] = access_sql
%
% aims to access the database file via sqlite. 
%
% Requires SQLite to be installed: http://www.sqlite.org/download.html
% 
%
% 2013.03.05 by John + RJE
% 2013 03.05 Updated by John


% get a file
[f p] = uigetfile('*.db',  'Select a *.db file');

filename = [p f];

% Open connection
SQLConnecter =  JDBC_MATLAB_CONNECT(filename);

query = 'SELECT id FROM songs limit 10'; % "query" is actual syntax for mksqlite

fprintf([' Processing the query <',query,'>. Please wait ...']);
% if you want to test, you can add 'limit N' at the end of the query
% now do the query
result = SQLConnecter.executeQuery(query);

nrows = size(result,1); % number of rows

% allow user to select a subset of indices
fprintf([' Finished. \n\n There are ' num2str(nrows) ' entries (rows).'])
sel_ind = input(' Which entries to process? \n [1] All <ENTER>; \n [2] Selection; \n --> ');

if isempty(sel_ind)
   sel_ind = 1;
end

if sel_ind == 1
   ind = 1:nrows;
   
elseif sel_ind == 2
   ind_start = input('    Starting index: ');
   ind_stop  = input('    Stopping index: ');
   ind = ind_start:ind_stop;

end

nind = numel(ind); % for the loop

% save variables

%id = 
%song_id = 
%album_id = 
name = cell(nind,1);
%artist_name = 
%title = 
%language = 

for i = 1 : nind
    
    j = ind(i); % the actual index VALUE
    
    % a query loop
    query = sprintf('SELECT * FROM Songs where id == %d',result{j}); % note: < SELECT * > is used to grab all fields
    
    song = SQLConnecter.executeQuery(query);
    
    name{i} = song{1}.name;
%*************************************************************************%
% song(struct) contains all fields. 'song' for the song with id == 1 is
% below, you can freely access any of the field. 
% For example, song.song_id returns the song_id of the song.
%  
% 
%              id: 1984210
%         song_id: '1000386'
%        album_id: '50038'
%            name: 'Sight for Sore Eyes'
%           style: []
%     artist_name: 'Aerosmith'
%       artist_id: '10003'
%           lyric: []
%       lyric_url: '0'
%      lyric_file: []
%           title: 'Draw the Line'
%        language: 'Ӣ��'
%         company: 'Columbia'
%      album_logo: 'http://img.xiami.com/./images/album/img3/10003/50038_2.jpg'
%      play_count: '573'
%        location: 'http://f1.xiami.net/10003/50038/08 1000386_126858.mp3'
%        low_size: '2833449'
%        low_hash: []
%       
%*************************************************************************%
end
SQLConnecter.close();
clear SQLConnecter;

% output

output.song_name = name;
