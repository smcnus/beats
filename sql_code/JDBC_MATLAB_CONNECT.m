classdef JDBC_MATLAB_CONNECT
%     The class provide connection to database via JDBC
%     PUBLIC METHODS: 
%       function conn = JDBC_MATLAB_CONNECT(databaseName): 
%             Constructor function, 
%             INPUT: 'databaseName' is the name of the *.db file you want to use;
%             OUTPUT: a real object of this class
%           
%       function result = executeQuery(query):
%             Execute the query,
%             INPUT: 'query' is the query you want to execute, make sure it is valid;
%             OUTPUT: 'result' is the search result from the database.
%                    ATTENTION: The type of result depends on what query you type in.
%                        For example, if query is 'SELECT * FROM SONGS WHERE ID == 2',
%                        'result' is a array of STRUCTURE with all the fileds in it.
%                        You can access any field like: 'result.id';
%
%                        HOWEVER, when the query is like 'SELECT name FROM SONGS WHERE ID==2',
%                        'result' is just a cell array, you can never access the fields like 'result.name',
%                        use 'result{1}' is OK.
%
%       function result = searchWithNameInPartFile(partFileName): use the name get
%           from the part file and search for more information in database.
%           'partFileName' is the name from the part file. 'result' is the
%           search result from the database.
%
%       function [] = close(): close the database if you don't want to use it   
%       ATTENTION: You'd better clear the object after you don't want to
%       use it any more.
    
    properties
        connHandle;
    end
    
    methods 
        function result = JDBC_MATLAB_CONNECT(databaseName)% I'm sorry you have to type in the complete path
            path = sprintf('jdbc:sqlite:%s',databaseName);
            result.connHandle = database(databaseName,'','','org.sqlite.JDBC',path);
        end
        
        function result = executeQuery(obj,query)
            curs = exec(obj.connHandle,query);
            data = fetch(curs);
            temp = regexp(query,'*','split');
            if(size(temp,2)==1);
                result = data.Data;
            else
                sizeOfResult = size(data.Data,1);
                result = cell(sizeOfResult,1);
                for i = 1 : sizeOfResult
                    result{i}.id = data.Data{1};
                    result{i}.song_id = data.Data{2};
                    result{i}.album_id = data.Data{3};
                    result{i}.name = data.Data{4};
                    result{i}.style = data.Data{5};
                    result{i}.artist_name = data.Data{6};
                    result{i}.artist_id = data.Data{7};
                    result{i}.lyric = data.Data{8};
                    result{i}.lyric_url = data.Data{9};
                    result{i}.lyric_file = data.Data{10};
                    result{i}.title = data.Data{11};
                    result{i}.language = data.Data{12};
                    result{i}.company = data.Data{13};
                    result{i}.album_logo = data.Data{14};
                    result{i}.play_count = data.Data{15};
                    result{i}.location = data.Data{16};
                    result{i}.low_size = data.Data{17};
                    result{i}.low_hash = data.Data{18};
                end
            end
            
        end
        
        function result = searchWithNameInPartFile(obj,partFileName)
            mat = regexp(partFileName,'_','split');
            mat = regexp(mat{1},' ','split');
            query = sprintf('SELECT * FROM Songs where song_id == %s',mat{2});
            result = executeQuery(obj,query);
        end
        
        function close(obj)
            % I don't know what to do actually...
        end
       
    end
end
