function fileList = findAllFiles_rje(dirName,tmatch,showprog)
% fileList = findAllFiles(dirName,tmatch)
%   returns a cell array containing the file names of all files
%   matching either a string ("tmatch") anywhere in the file name 
%   below directory <dirName>.
%
% THIS IS MODIFIED FROM THIS BLOG POST:
% http://stackoverflow.com/questions/2652630/how-to-get-all-files-under-a-specific-directory-in-matlab
% THIS IS MATLAB DONE BY SOMEONE THAT DOES NOT LIKE MATLAB ;)
% We welcome any sort of improvement! -TBM
%
% 2011-01-27 dpwe@ee.columbia.edu tb2332@columbia.edu
%
% modified by rje | 2013.03.15

if (nargin < 2); 
  fprintf('\n Error: specify "tmatch" string to identify files.\n\n')
end

dirData = dir(dirName);     % Get the data for the current directory


dirIndex = [dirData.isdir];  % Find the index for directories
fileListTmp = {dirData(~dirIndex).name}';  %'# Get a list of the files


fileList = {};

% keep files with the right extension
for k=1:numel(fileListTmp)

  if sum(findstr(fileListTmp{k},tmatch)) > 0 % findstr returns indices and will be [ ] if it fails
    fileList = cat(1,fileList,fileListTmp(k)); % rje changed this
  end

end

if ~isempty(fileList)
  fileList = cellfun(@(x) fullfile(dirName,x),... % Prepend path to files
                     fileList,'UniformOutput',false);
end

% cnt
cnt = size(fileList,1);

subDirs = {dirData(dirIndex).name};  % Get a list of the subdirectories
validIndex = ~ismember(subDirs,{'.','..'});  % Find index of subdirs
                                             %  that are not '.' or '..'
% Loop over valid subdirectories
for iDir = find(validIndex)
  if showprog == 1
      fprintf('####################') % RJE: will print this every time a subdirectory inside directory dirName is reached
      pause(.01)
      fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b')
  end
  
  % Get the subdirectory path
  % Recursively call findAllFiles
  sublist = findAllFiles_rje([dirName,filesep,subDirs{iDir}],tmatch,showprog); % recursive
  fileList = [fileList;sublist];
  cnt = cnt + length(sublist);
end






