![iBEATS Logo](https://bytebucket.org/smcnus/beats/raw/fb2bb404b42f501ca26dc50cc5f23a059872f552/iBEATS_logo.png)

[iBEATS Website](http://ibeats.smcnus.org)

Matlab code for "Balanced evaluation of auditory temporal stability (BEATS)"
To appear in PLOS ONE

* In addition to the suite of .m functions included in this folder, you will also need to 
  download and install the following:

	1. [The Million Song Dataset (MSD)](http://labrosa.ee.columbia.edu/millionsong/)
	2. [Additional MSD scripts](http://labrosa.ee.columbia.edu/millionsong/pages/matlab-introduction)

* Note: BEATS works best when the original .h5 files from the MSD are first converted 
  to .mat format. A few  samples in this converted format are included 
  in the /examples folder (these are the files which comprise Figure 3 in the paper)

* BEATS also requires a few functions obtained from [Matlab Central](http://www.mathworks.com/matlabcentral):

	1. [progressbar.m](http://www.mathworks.com/matlabcentral/fileexchange/6922-progressbar)
	2. [kernel density estimator for central tendency](http://www.mathworks.com/matlabcentral/fileexchange/14034-kernel-density-estimator)

* TO RUN BEATS

	 1. `>> beats`
	 2. <ENTER> for converted .h5 files
	 3. <ENTER> for GUI mode (uses text prompts and allows for figure plots)
	 4. Select the directly of the converted (.h5 --> .mat) files (i.e., /examples)
	 5. <ENTER> to set the initialization thresholds
	 6. <ENTER> for 5
	 7. <ENTER> for 10
	 8. <ENTER> for 2.5
	 9. Select [2] for simplest plotting option
	10. Select [1] to pause between files to allow viewing
	11. <ENTER> for unlimited viewing time
	12. <ENTER> to select color figures
	13. <ENTER> to not save results (can do manually later)
	14. <ENTER> to not send email (useful if running a large batch to alert user when finished)


Rob Ellis  
11 November 2014  
robjellis@gmail.com
